module.exports = {
  i18n: {
    defaultLocale: 'ja',
    locales: ['ja', 'en', 'ko', 'zh'],
  },
}
