export default {
  common: {
    preparing: '현재 준비중',
  },
  layout: {
    title: {
      home: '가정',
      employee: '종업원 검색',
      engineer: '중요 엔지니어 나열',
      customer: '고객 별 예산 관리',
      proposition: '안건 정보',
      price: '단가 나열',
      setting: '설정',
    },
    dropdown: {
      setting: '설정',
      logout: '로그 아웃',
    },
  },
  settings: {
    tabTitle1: '프로필',
    tabTitle2: '기타',
    profile: {
      inputText1: '별명',
      inputText2: '도도부 현',
      inputText3: '시구 정촌',
      inputText4: '제일 가까운 역',
      imageText1: '사진을 업로드',
      imageText2: '기본 이미지를 설정',
    },
    password: {
      inputText1: '현재 비밀번호',
      inputText2: '새 암호',
      inputText3: '새 암호 확인',
    },
    other: {
      selectText: '언어',
      inputText1: '검색 태그',
      inputText2: '포트폴리오 URL',
    },
    buttonText: '업데이트',
  },
}
