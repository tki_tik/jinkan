export default {
  common: {
    preparing: '目前正在准备',
  },
  layout: {
    title: {
      home: '家',
      employee: '员工搜寻',
      engineer: '重要工程师名单',
      customer: '客户预算管理',
      proposition: '事项信息',
      price: '单价清单',
      setting: '设定',
    },
    dropdown: {
      setting: '设定',
      logout: '登出',
    },
  },
  settings: {
    tabTitle1: '自我介绍',
    tabTitle2: '其他',
    profile: {
      inputText1: '昵称',
      inputText2: '都道府县',
      inputText3: '市区县村',
      inputText4: '最近的车站',
      imageText1: '上传图片',
      imageText2: '设为默认图片',
    },
    password: {
      inputText1: '当前密码',
      inputText2: '新密码',
      inputText3: '确认新密码',
    },
    other: {
      selectText: '语言',
      inputText1: '搜索标签',
      inputText2: '作品集URL',
    },
    buttonText: '更新',
  },
}
