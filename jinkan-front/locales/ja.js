export default {
  common: {
    preparing: '現在準備中',
  },
  layout: {
    title: {
      home: 'ホーム',
      employee: '従業員検索',
      engineer: '重要技術者一覧',
      customer: '顧客別予算管理',
      proposition: '案件情報',
      price: '単価一覧',
      setting: '設定',
    },
    dropdown: {
      setting: '設定',
      logout: 'ログアウト',
    },
  },
  settings: {
    tabTitle1: 'プロフィール',
    tabTitle2: 'その他',
    profile: {
      inputText1: 'ニックネーム',
      inputText2: '都道府県',
      inputText3: '市区町村',
      inputText4: '最寄駅',
      imageText1: '画像をアップロード',
      imageText2: 'デフォルト画像に設定',
    },
    password: {
      inputText1: '現在のパスワード',
      inputText2: '新しいパスワード',
      inputText3: '新しいパスワードの確認',
    },
    other: {
      selectText: '言語',
      inputText1: '検索タグ（習得言語等）',
      inputText2: 'ポートフォリオURL',
    },
    buttonText: '更新',
  },
}
