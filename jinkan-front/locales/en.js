export default {
  common: {
    preparing: 'Currently in preparation',
  },
  layout: {
    title: {
      home: 'Home',
      employee: 'Employee search',
      engineer: 'Engineer list',
      customer: 'Customer badget',
      proposition: 'Proposition info',
      price: 'Price list',
      setting: 'Settings',
    },
    dropdown: {
      setting: 'Settings',
      logout: 'Logout',
    },
  },
  settings: {
    tabTitle1: 'Profile',
    tabTitle2: 'Other',
    profile: {
      inputText1: 'Nickname',
      inputText2: 'State',
      inputText3: 'City',
      inputText4: 'Near station',
      imageText1: 'Upload image',
      imageText2: 'Set default image',
    },
    password: {
      inputText1: 'Current password',
      inputText2: 'New password',
      inputText3: 'Confirm new password',
    },
    other: {
      selectText: 'Language',
      inputText1: 'Search tag',
      inputText2: 'Portfolio url',
    },
    buttonText: 'Update',
  },
}
