import dayjs from 'dayjs'

export const diff = (
  dateFrom: Date,
  dateTo: Date,
  unit: 'month' | 'day' | 'hour'
): number => {
  const from = dayjs(dateFrom)
  const to = dayjs(dateTo)
  return Math.abs(to.diff(from, unit))
}

export const formatDate = (target: Date, format: string): string => {
  return dayjs(target).format(format)
}

export const toDate = (dateStr: string, format: string): Date => {
  return dayjs(dateStr, format).toDate()
}

export const calcAge = (birthday) => {
  const today = dayjs()
  const birthDate = dayjs(birthday)
  const baseAge = today.year() - birthDate.year()
  const thisBirthday = dayjs(
    `${today.year()}-${birthDate.month() + 1}-${birthDate.date()}`
  )
  return today.isBefore(thisBirthday) ? baseAge - 1 : baseAge
}
