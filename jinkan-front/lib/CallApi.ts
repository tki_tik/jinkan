import { Dispatch, SetStateAction } from 'react'
import Router from 'next/router'
import axios, { AxiosInstance, AxiosResponse } from 'axios'
import Cookie from 'universal-cookie'

const cookie = new Cookie()

export interface Response {
  status: 'success' | 'error'
  data: any
}

export const httpClient = (
  token: string,
  contentType: string
): AxiosInstance => {
  return axios.create({
    headers: {
      Authorization: token,
      contentType: contentType,
    },
  })
}

export const callApi = async <T>(
  axiosFunc: () => Promise<AxiosResponse<T>>,
  initialData: T,
  setIsLoading: Dispatch<SetStateAction<boolean>>
): Promise<Response> => {
  try {
    setIsLoading && setIsLoading(true)
    const res = await axiosFunc()
    if (res.status === 200) {
      cookie.set('token', res.headers['authorization'])
      setIsLoading && setIsLoading(false)
      return {
        status: 'success',
        data: res.data,
      }
    }
  } catch (error) {
    cookie.remove('token')
    if (error.response) {
      if (error.response.status === 401) {
        Router.push({
          pathname: '/error',
          query: {
            code: error.response.status,
            title: 'Session Timeout',
          },
        })
      } else {
        Router.push({
          pathname: '/error',
          query: {
            code: error.response.status,
          },
        })
      }
    } else {
      Router.push({
        pathname: '/error',
        query: {
          title: error.message,
        },
      })
    }
    return {
      status: 'error',
      data: initialData,
    }
  }
}
