import Cookie from 'universal-cookie'
import AuthInfo from '../types/AuthInfo'

const cookie: Cookie = new Cookie()

export const getUserId = (): string => {
  return getPayload().sub
}

export const getUsername = (): string => {
  return getPayload().username
}

export const getRole = (): string => {
  return getPayload().role
}

const getPayload = (): AuthInfo => {
  const token: string = cookie.get('token')
  const base64Payload = token.split('.')[1].replace('-', '+').replace('_', '/')
  const decodePayload = decodeURIComponent(escape(atob(base64Payload)))
  const payload = JSON.parse(decodePayload)
  return payload
}
