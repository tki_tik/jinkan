import { useToasts } from 'react-toast-notifications'

export const useToast = () => {
  const { addToast } = useToasts()

  const success = (message: string) => {
    addToast(message, {
      appearance: 'success',
      autoDismiss: true,
      transitionDuration: 1000,
    })
  }

  const error = (message: string) => {
    addToast(message, {
      appearance: 'error',
      autoDismiss: true,
      transitionDuration: 1000,
    })
  }

  return { success, error }
}
