import { useRouter } from 'next/router'
import en from '../locales/en'
import ja from '../locales/ja'
import ko from '../locales/ko'
import zh from '../locales/zh'

const getTranslation = (locale: string) => {
  if (locale === 'en') {
    return en
  } else if (locale === 'ko') {
    return ko
  } else if (locale === 'zh') {
    return zh
  } else {
    return ja
  }
}

export const useLocale = () => {
  const { locale } = useRouter()

  const t = getTranslation(locale)

  return { locale, t }
}
