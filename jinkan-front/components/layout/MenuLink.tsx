import Link from 'next/link'
import { useLocale } from '../../hook/useLocale'
import MenuInfo from '../../types/MenuInfo'

interface Props {
  menu: MenuInfo
  activeMenu: string
}

const MenuLink: React.FC<Props> = ({ menu, activeMenu }) => {
  const { t } = useLocale()

  return (
    <Link href={menu.url}>
      <a
        className={`flex items-center mt-4 py-2 px-6 hover:bg-gray-700 hover:bg-opacity-25 hover:text-gray-100 ${
          menu.titleTag === activeMenu
            ? 'bg-gray-700 text-gray-100'
            : 'text-gray-500'
        }`}
      >
        <svg
          className="h-6 w-6"
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            d={menu.path}
          ></path>
        </svg>
        <span className="mx-3">{t.layout.title[menu.titleTag]}</span>
      </a>
    </Link>
  )
}

export default MenuLink
