import Image from 'next/image'
import MenuLink from './MenuLink'
import MenuInfo from '../../types/MenuInfo'

interface Props {
  isOpen: boolean
  title: string
  menuInfoList: Array<MenuInfo>
}

const Slidebar: React.FC<Props> = ({ isOpen, title, menuInfoList }) => {
  return (
    <div
      className={`fixed z-30 inset-y-0 left-0 w-64 transition duration-300 transform bg-gray-900 overflow-y-auto lg:translate-x-0 lg:static lg:inset-0 ${
        isOpen ? 'translate-x-0 ease-out' : '-translate-x-full ease-in'
      }`}
    >
      <div className="flex items-center justify-center mt-8">
        <div className="flex items-center">
          <Image src="/img/jinkan-logo.svg" width={200} height={67} />
        </div>
      </div>
      <nav className="mt-10">
        {menuInfoList &&
          menuInfoList.map((menu) => (
            <MenuLink menu={menu} activeMenu={title} key={menu.id} />
          ))}
      </nav>
    </div>
  )
}

export default Slidebar
