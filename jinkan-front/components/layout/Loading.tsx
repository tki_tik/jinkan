import Image from 'next/image'

const Loading: React.FC = () => {
  return (
    <div className="w-full h-full flex flex-col absolute z-40 bg-gray-300 bg-opacity-50">
      <div className="w-12 h-12 m-auto">
        <Image
          src={'/img/loading.gif'}
          width={48}
          height={48}
          layout={'fixed'}
        />
      </div>
    </div>
  )
}

export default Loading
