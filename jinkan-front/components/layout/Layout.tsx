import { useEffect, useState } from 'react'
import Head from 'next/head'
import { useRouter } from 'next/router'
import Cookie from 'universal-cookie'
import Header from './Header'
import Loading from './Loading'
import Slidebar from './Slidebar'
import { useStateContext } from '../../context/StateProvider'
import GetMenuInfoList from '../../fetch/GetMenuInfoList'
import GetUserInfo from '../../fetch/GetUserInfo'
import RefreshToken from '../../fetch/RefreshToken'
import { useLocale } from '../../hook/useLocale'
import { getRole } from '../../lib/GetAuthInfo'

const cookie = new Cookie()

interface Props {
  title: string
  authList: Array<string>
}

const Layout: React.FC<Props> = ({ children, title, authList }) => {
  const { t } = useLocale()
  const { push, pathname, asPath } = useRouter()
  const {
    isLoading,
    initializelUserInfo,
    setIsLoading,
    setTempUserInfo,
    setUserInfo,
  } = useStateContext()
  const [isLogin, setIsLogin] = useState(false)
  const [isSidebarOpen, setIsSidebarOpen] = useState(false)
  const [menuInfoList, setMenuInfoList] = useState([])

  const getInitInfo = async (token: string) => {
    const menuRes = await GetMenuInfoList(token, setIsLoading)
    if (menuRes.status == 'error') {
      return
    }
    setMenuInfoList(menuRes.data.menuInfoList)
    const userRes = await GetUserInfo(token, setIsLoading)
    if (userRes.status == 'error') {
      return
    }
    setTempUserInfo(userRes.data)
    setUserInfo(userRes.data)
    push(pathname, asPath, { locale: userRes.data.locale })
  }

  useEffect(() => {
    const token = cookie.get('token')

    if (token && authList.includes(getRole())) {
      getInitInfo(token)
      setIsLogin(true)
    } else if (!token) {
      push(
        `${process.env.NEXT_PUBLIC_JINKAN_RESTAPI_URL}/auth/redirect-login-api`
      )
    } else {
      cookie.remove('token')
      push({
        pathname: '/error',
        query: {
          code: 404,
        },
      })
    }
  }, [])

  useEffect(() => {
    setIsSidebarOpen(false)
    isLogin && RefreshToken(cookie.get('token'))
    initializelUserInfo()
  }, [title])

  return (
    <>
      <Head>
        <title>{t.layout.title[title]}</title>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="author" content="tki_tik" />
      </Head>
      <div className="bg-white h-screen text-gray-600 font-mono">
        {isLogin && (
          <div className="flex h-screen bg-gray-200">
            {isLoading && <Loading />}
            <div
              onClick={() => setIsSidebarOpen(false)}
              className={`fixed z-20 inset-0 bg-black opacity-50 transition-opacity lg:hidden ${
                isSidebarOpen ? 'block' : 'hidden'
              }`}
            ></div>
            <Slidebar
              isOpen={isSidebarOpen}
              title={title}
              menuInfoList={menuInfoList}
            />
            <div className="flex-1 flex flex-col overflow-hidden">
              <Header title={title} setIsSidebarOpen={setIsSidebarOpen} />
              <main className="flex-1 overflow-x-hidden overflow-y-auto bg-gray-100">
                {children}
              </main>
            </div>
          </div>
        )}
      </div>
    </>
  )
}

export default Layout
