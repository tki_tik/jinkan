import { useState } from 'react'
import Link from 'next/link'
import Image from 'next/image'
import { useRouter } from 'next/router'
import Cookie from 'universal-cookie'
import { useStateContext } from '../../context/StateProvider'
import Logout from '../../fetch/Logout'
import { useLocale } from '../../hook/useLocale'
import { getUsername } from '../../lib/GetAuthInfo'

const cookie = new Cookie()

interface Props {
  title: string
  setIsSidebarOpen: (isSidebarOpen: boolean) => void
}

const Header: React.FC<Props> = ({ title, setIsSidebarOpen }) => {
  const { t } = useLocale()
  const router = useRouter()
  const { profileImageUrl, tempUserInfo } = useStateContext()
  const [isDropdownOpen, setIsDropdownOpen] = useState(false)

  const logout = async () => {
    await Logout(cookie.get('token'))
    await cookie.remove('token')
    router.reload()
  }

  return (
    <header className="flex justify-between items-center py-4 px-6 bg-white border-b-4 border-black">
      <div className="flex items-center">
        <button
          onClick={() => setIsSidebarOpen(true)}
          className="text-gray-500 focus:outline-none lg:hidden"
        >
          <svg
            className="h-6 w-6"
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M4 6H20M4 12H20M4 18H11"
              stroke="currentColor"
              strokeWidth="2"
              strokeLinecap="round"
              strokeLinejoin="round"
            ></path>
          </svg>
        </button>
      </div>
      <div className="lg:w-full lg:text-left text-xl">
        <span>{t.layout.title[title]}</span>
      </div>
      <div className="flex items-center">
        <div className="relative">
          <button
            onClick={() => setIsDropdownOpen(!isDropdownOpen)}
            className="relative block h-8 w-8 rounded-full overflow-hidden shadow focus:outline-none"
          >
            <Image
              src={profileImageUrl ? profileImageUrl : '/img/profile.svg'}
              layout="fill"
            />
          </button>
          <div
            onClick={() => setIsDropdownOpen(false)}
            className={`fixed inset-0 h-full w-full z-10 ${
              isDropdownOpen ? '' : 'hidden'
            }`}
          ></div>
          <div
            className={`absolute right-0 mt-2 bg-white rounded-md overflow-hidden shadow-xl z-10 ${
              isDropdownOpen ? '' : 'hidden'
            }`}
          >
            <Link href="/">
              <a
                onClick={() => setIsDropdownOpen(false)}
                className="block p-2 text-sm text-gray-700 hover:bg-gray-900 hover:text-white"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="absolute h-5 w-5"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"
                  />
                </svg>
                <span className="block w-36 pl-6 truncate">
                  {tempUserInfo.dispNm ? tempUserInfo.dispNm : getUsername()}
                </span>
              </a>
            </Link>
            <Link href="/settings">
              <a
                onClick={() => setIsDropdownOpen(false)}
                className="block p-2 text-sm text-gray-700 hover:bg-gray-900 hover:text-white"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="absolute h-5 w-5"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"
                  />
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                  />
                </svg>
                <span className="pl-6">{t.layout.dropdown.setting}</span>
              </a>
            </Link>
            <a
              onClick={logout}
              className="block p-2 text-sm text-gray-700 cursor-pointer hover:bg-gray-900 hover:text-white"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="absolute h-5 w-5"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1"
                />
              </svg>
              <span className="pl-6">{t.layout.dropdown.logout}</span>
            </a>
          </div>
        </div>
      </div>
    </header>
  )
}

export default Header
