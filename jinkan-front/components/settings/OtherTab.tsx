import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import Cookie from 'universal-cookie'
import Button from '../common/Button'
import Input from '../common/Input'
import Select from '../common/Select'
import { useStateContext } from '../../context/StateProvider'
import GetUserInfo from '../../fetch/GetUserInfo'
import UpdateOtherData from '../../fetch/UpdateOtherData'
import { useLocale } from '../../hook/useLocale'
import { useToast } from '../../hook/useToast'
import { getUserId } from '../../lib/GetAuthInfo'
import InputInfo from '../../types/InputInfo'
import SelectInfo from '../../types/SelectInfo'

const cookie = new Cookie()

const OtherTab: React.FC = () => {
  const { t } = useLocale()
  const { push, pathname, asPath } = useRouter()
  const {
    locale,
    portfolioUrl,
    searchTag,
    tempUserInfo,
    setLocale,
    setIsLoading,
    setPortfolioUrl,
    setSearchTag,
    setTempUserInfo,
    setUserInfo,
  } = useStateContext()
  const { success } = useToast()
  const [isBtnDisabled, setIsBtnDisabled] = useState(true)

  const selectInfo: SelectInfo = {
    name: 'language',
    itemNm: t.settings.other.selectText,
    value: locale,
    setValue: setLocale,
    optionList: [
      {
        name: '日本語',
        value: 'ja',
      },
      {
        name: 'English',
        value: 'en',
      },
      {
        name: '한국어',
        value: 'ko',
      },
      {
        name: '简体中文',
        value: 'zh',
      },
    ],
  }
  const inputInfoList: Array<InputInfo> = [
    {
      id: 'tag',
      type: 'text',
      itemNm: t.settings.other.inputText1,
      value: searchTag ? searchTag : '',
      setValue: setSearchTag,
      isRequired: false,
    },
    {
      id: 'portfolio_url',
      type: 'url',
      itemNm: t.settings.other.inputText2,
      value: portfolioUrl ? portfolioUrl : '',
      setValue: setPortfolioUrl,
      isRequired: false,
    },
  ]

  const updateOther = async () => {
    const token = cookie.get('token')
    const otherInfo = {
      userId: getUserId(),
      locale: locale,
      searchTag: searchTag,
      portfolioUrl: portfolioUrl,
    }
    const res = await UpdateOtherData(token, otherInfo, setIsLoading)
    if (res.status == 'error') {
      return
    }
    const userRes = await GetUserInfo(token, setIsLoading)
    if (userRes.status == 'error') {
      return
    }
    setTempUserInfo(userRes.data)
    setUserInfo(userRes.data)
    push(pathname, asPath, { locale: userRes.data.locale })
    setIsBtnDisabled(true)
    success('その他情報を更新しました')
  }

  const isChangeProfile = (): boolean => {
    return (
      locale != tempUserInfo.locale ||
      searchTag != tempUserInfo.searchTag ||
      portfolioUrl != tempUserInfo.portfolioUrl
    )
  }

  useEffect(() => {
    if (isChangeProfile()) {
      setIsBtnDisabled(false)
    } else {
      setIsBtnDisabled(true)
    }
  }, [locale, portfolioUrl, searchTag])

  return (
    <>
      <div className="my-5">
        <Select {...selectInfo} />
      </div>
      {inputInfoList.map((inputInfo, index) => (
        <div className="mb-5" key={index}>
          <Input {...inputInfo} />
        </div>
      ))}
      <div className="my-5">
        <Button
          value={t.settings.buttonText}
          onClick={updateOther}
          isDisabled={isBtnDisabled}
        />
      </div>
    </>
  )
}

export default OtherTab
