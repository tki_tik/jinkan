import { useEffect, useState } from 'react'
import Cookie from 'universal-cookie'
import Button from '../common/Button'
import Input from '../common/Input'
import ProfileImage from './ProfileImage'
import { useStateContext } from '../../context/StateProvider'
import GetUserInfo from '../../fetch/GetUserInfo'
import UpdateProfileData from '../../fetch/UpdateProfileData'
import { useLocale } from '../../hook/useLocale'
import { useToast } from '../../hook/useToast'
import { getUserId, getUsername } from '../../lib/GetAuthInfo'
import InputInfo from '../../types/InputInfo'

const cookie = new Cookie()

const ProfileTab: React.FC = () => {
  const { t } = useLocale()
  const {
    city,
    dispNm,
    state,
    station,
    tempUserInfo,
    setCity,
    setDispNm,
    setIsLoading,
    setState,
    setStation,
    setTempUserInfo,
    setUserInfo,
  } = useStateContext()
  const { success } = useToast()
  const [isBtnDisabled, setIsBtnDisabled] = useState(true)

  const inputInfoList: Array<InputInfo> = [
    {
      id: 'name',
      type: 'text',
      itemNm: t.settings.profile.inputText1,
      value: tempUserInfo.dispNm ? dispNm : getUsername(),
      setValue: setDispNm,
      isRequired: true,
    },
    {
      id: 'state',
      type: 'text',
      itemNm: t.settings.profile.inputText2,
      value: state,
      setValue: setState,
      isRequired: true,
    },
    {
      id: 'city',
      type: 'text',
      itemNm: t.settings.profile.inputText3,
      value: city,
      setValue: setCity,
      isRequired: true,
    },
    {
      id: 'station',
      type: 'text',
      itemNm: t.settings.profile.inputText4,
      value: station,
      setValue: setStation,
      isRequired: true,
    },
  ]

  const updateProfile = async () => {
    const token = cookie.get('token')
    const profileInfo = {
      userId: getUserId(),
      dispNm: dispNm,
      state: state,
      city: city,
      station: station,
    }
    const res = await UpdateProfileData(token, profileInfo, setIsLoading)
    if (res.status == 'error') {
      return
    }
    const userRes = await GetUserInfo(token, setIsLoading)
    if (userRes.status == 'error') {
      return
    }
    setTempUserInfo(userRes.data)
    setUserInfo(userRes.data)
    setIsBtnDisabled(true)
    success('プロフィールを更新しました')
  }

  const isInputRequired = (): boolean => {
    return !(!dispNm || !state || !city || !station)
  }

  const isChangeProfile = (): boolean => {
    return (
      dispNm != tempUserInfo.dispNm ||
      state != tempUserInfo.state ||
      city != tempUserInfo.city ||
      station != tempUserInfo.station
    )
  }

  useEffect(() => {
    if (isInputRequired() && isChangeProfile()) {
      setIsBtnDisabled(false)
    } else {
      setIsBtnDisabled(true)
    }
  }, [city, dispNm, state, station])

  return (
    <>
      <ProfileImage />
      {inputInfoList.map((inputInfo, index) => (
        <div className="mb-5" key={index}>
          <Input {...inputInfo} />
        </div>
      ))}
      <div className="my-5">
        <Button
          value={t.settings.buttonText}
          onClick={updateProfile}
          isDisabled={isBtnDisabled}
        />
      </div>
    </>
  )
}

export default ProfileTab
