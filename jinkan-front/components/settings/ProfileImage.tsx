import { useEffect, useState } from 'react'
import Image from 'next/image'
import Cookie from 'universal-cookie'
import { FileUpload, useFileUpload } from 'use-file-upload'
import { useStateContext } from '../../context/StateProvider'
import UploadProfileImage from '../../fetch/UploadProfileImage'
import { useLocale } from '../../hook/useLocale'
import { useToast } from '../../hook/useToast'

const cookie = new Cookie()

const ProfileImage: React.FC = () => {
  const { t } = useLocale()
  const { profileImageUrl, setIsLoading, setProfileImageUrl } =
    useStateContext()
  const [file, selectFile] = useFileUpload()
  const { success, error } = useToast()
  const [isDropdownOpen, setIsDropdownOpen] = useState(false)

  const uploadSelectFile = () => {
    setIsDropdownOpen(false)
    selectFile({ accept: '', multiple: false }, () => {})
  }

  const uploadDefaultFile = () => {
    setIsDropdownOpen(false)
    uploadProfileImage(null)
  }

  const uploadProfileImage = async (image: FileUpload) => {
    const errorMessage = validateFile(image)
    if (errorMessage !== '') {
      error(`画像更新失敗：${errorMessage}`)
      return
    }
    const res = await UploadProfileImage(
      cookie.get('token'),
      image,
      setIsLoading
    )
    if (res.status === 'success') {
      setProfileImageUrl(res.data)
      success('画像を更新しました')
    }
  }

  const validateFile = (image: FileUpload): string => {
    const validFileTypes: Array<string> = [
      'image/jpeg',
      'image/png',
      'image/svg+xml',
    ]
    const sizeLimit: number = 2097152
    if (image && !validFileTypes.includes(image.file.type)) {
      return 'ファイル拡張子不正'
    }
    if (image && image.size > sizeLimit) {
      return 'サイズ上限(2MB)オーバー'
    }
    return ''
  }

  useEffect(() => {
    if (file) {
      uploadProfileImage(file as FileUpload)
    }
  }, [file])

  return (
    <div className="relative mb-5 text-center">
      <button
        onClick={() => setIsDropdownOpen(true)}
        className="relative h-32 w-32 focus:outline-none"
      >
        <Image
          className="rounded"
          src={profileImageUrl ? profileImageUrl : '/img/profile.svg'}
          layout="fill"
        />
        <div className="bg-cyan-400 h-8 w-8 rounded-full absolute bottom-1 right-1 flex items-center justify-center">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5 text-white"
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path
              fillRule="evenodd"
              d="M4 5a2 2 0 00-2 2v8a2 2 0 002 2h12a2 2 0 002-2V7a2 2 0 00-2-2h-1.586a1 1 0 01-.707-.293l-1.121-1.121A2 2 0 0011.172 3H8.828a2 2 0 00-1.414.586L6.293 4.707A1 1 0 015.586 5H4zm6 9a3 3 0 100-6 3 3 0 000 6z"
              clipRule="evenodd"
            />
          </svg>
        </div>
      </button>
      <div
        onClick={() => setIsDropdownOpen(false)}
        className={`fixed inset-0 h-full w-full z-10 ${
          isDropdownOpen ? '' : 'hidden'
        }`}
      ></div>
      <div
        className={`absolute w-44 mt-2 bg-white rounded-md overflow-hidden shadow-xl z-10 top-1/2 left-1/2 transform -translate-x-1/2 ${
          isDropdownOpen ? '' : 'hidden'
        }`}
      >
        <a
          onClick={uploadSelectFile}
          className="block p-4 text-sm text-gray-700 hover:bg-gray-900 hover:text-white cursor-pointer"
        >
          <span>{t.settings.profile.imageText1}</span>
        </a>
        {profileImageUrl && (
          <a
            onClick={uploadDefaultFile}
            className="block p-4 text-sm text-gray-700 hover:bg-gray-900 hover:text-white cursor-pointer"
          >
            <span>{t.settings.profile.imageText2}</span>
          </a>
        )}
      </div>
    </div>
  )
}

export default ProfileImage
