import Image from 'next/image'
import ProfileInfo from '../../types/ProfileInfo'

const ProfileMini: React.FC<ProfileInfo> = ({
  profileImageUrl,
  userId,
  userName,
}) => {
  return (
    <div className="flex flex-col w-11/12 justify-center mx-auto pb-2">
      <div className="relative h-8 w-8 m-2">
        <Image
          className="rounded"
          src={profileImageUrl ? profileImageUrl : '/img/profile.svg'}
          layout="fill"
        />
      </div>
      <div className="relative -top-12 -mb-12 pl-12">
        <p className="text-xs mt-2 text-gray-400">{`社員番号：${userId}`}</p>
        <p className="text-xs truncate">{userName}</p>
      </div>
    </div>
  )
}

export default ProfileMini
