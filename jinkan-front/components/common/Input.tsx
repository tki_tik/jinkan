import { useState } from 'react'
import InputInfo from '../../types/InputInfo'

const Input: React.FC<InputInfo> = (inputInfo) => {
  const [isFocus, setIsFocus] = useState(false)

  return (
    <div className="relative">
      <input
        id={inputInfo.id}
        type={inputInfo.type}
        autoComplete="off"
        value={inputInfo.value}
        onChange={(e) => inputInfo.setValue(e.target.value)}
        onFocus={() => setIsFocus(true)}
        onBlur={() => setIsFocus(false)}
        className="w-full h-12 px-4 border rounded text-gray-500 focus:border-cyan-400 focus:outline-none"
      />
      <label
        htmlFor={inputInfo.id}
        className={`absolute left-2 bg-white px-1 transform -translate-y-1/2 duration-300 ${
          isFocus ? 'text-cyan-400' : ''
        } ${
          inputInfo.value === '' && !isFocus
            ? 'top-1/2 text-sm text-gray-300'
            : 'top-0 text-xs text-gray-500'
        }`}
      >
        {inputInfo.itemNm}
        {inputInfo.isRequired && (
          <span
            className={inputInfo.value === '' && !isFocus ? '' : 'text-red-600'}
          >
            {' '}
            *
          </span>
        )}
      </label>
    </div>
  )
}

export default Input
