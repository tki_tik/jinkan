import SearchInfo from '../../types/SearchInfo'

const Search: React.FC<SearchInfo> = (info) => {
  return (
    <div className="relative mx-auto text-gray-500">
      <input
        className="w-full border-2 border-gray-300 bg-white h-12 pl-4 pr-8 rounded-lg text-xs focus:outline-none"
        type="search"
        name="search"
        value={info.value}
        onChange={(e) => info.setValue(e.target.value)}
        placeholder={info.placeholder}
      />
      <button
        onClick={() => info.onClick()}
        className="absolute right-0 top-0 mt-4 mr-4 focus:outline-none"
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="text-gray-600 h-4 w-4"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={2}
            d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
          />
        </svg>
      </button>
    </div>
  )
}

export default Search
