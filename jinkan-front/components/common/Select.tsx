import { useState } from 'react'
import SelectInfo from '../../types/SelectInfo'

const Select: React.FC<SelectInfo> = (selectInfo) => {
  const [isFocus, setIsFocus] = useState(false)

  return (
    <div className="relative z-0">
      <select
        name={selectInfo.name}
        value={selectInfo.value}
        onChange={(e) => selectInfo.setValue(e.target.value)}
        onFocus={() => setIsFocus(true)}
        onBlur={() => setIsFocus(false)}
        className="w-full h-12 px-4 border rounded text-gray-500 appearance-none cursor-pointer z-1 bg-transparent focus:border-cyan-400 focus:outline-none"
      >
        {selectInfo.optionList.map((option, index) => (
          <option key={index} value={option.value}>
            {option.name}
          </option>
        ))}
      </select>
      <label
        htmlFor={selectInfo.name}
        className={`absolute bg-white left-2 px-1 transform -translate-y-1/2 duration-300 ${
          isFocus ? 'text-cyan-400' : ''
        } ${
          selectInfo.value === '' && !isFocus
            ? 'top-1/2 text-sm text-gray-300 -z-1'
            : 'top-0 text-xs text-gray-500'
        }`}
      >
        {selectInfo.itemNm}
      </label>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        className={`${
          isFocus ? 'text-cyan-400' : 'text-gray-300'
        } h-5 w-5 absolute right-2 top-3.5 -z-1`}
        fill="none"
        viewBox="0 0 24 24"
        stroke="currentColor"
      >
        <path
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth={2}
          d="M19 9l-7 7-7-7"
        />
      </svg>
    </div>
  )
}

export default Select
