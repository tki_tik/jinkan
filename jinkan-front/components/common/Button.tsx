interface Props {
  value: string
  onClick: () => void
  isDisabled: boolean
}

const Button: React.FC<Props> = ({ value, onClick, isDisabled }) => {
  return (
    <button
      onClick={onClick}
      disabled={isDisabled}
      className={`w-full h-12 rounded text-white focus:outline-none ${
        isDisabled ? 'cursor-default bg-gray-200' : 'bg-cyan-400'
      }`}
    >
      {value}
    </button>
  )
}

export default Button
