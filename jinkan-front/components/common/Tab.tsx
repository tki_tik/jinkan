import TabInfo from '../../types/TabInfo'

const Tab: React.FC<TabInfo> = (tabInfo) => {
  return (
    <ul className="md:flex justify-center items-center mt-4 mb-8">
      {tabInfo.tabList.map((tab, index) => (
        <li
          key={index}
          onClick={() => tabInfo.setActiveTab(index)}
          className={`${
            tabInfo.activeTab === index
              ? 'border-cyan-400 text-cyan-400'
              : 'border-gray-200 text-gray-500'
          } ${
            tabInfo.tabList.length === 1
              ? 'md:w-1/1'
              : tabInfo.tabList.length === 2
              ? 'md:w-1/2'
              : 'md:w-1/3'
          } text-center cursor-pointer p-4 border-b-3 hover:border-cyan-400`}
        >
          {tab}
        </li>
      ))}
    </ul>
  )
}

export default Tab
