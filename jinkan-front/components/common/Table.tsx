import TableInfo from '../../types/TableInfo'

const Table: React.FC<TableInfo> = (tableInfo) => {
  return (
    <div className="w-11/12 mt-2 mx-auto border rounded">
      <div className="bg-gray-100">
        <p className="text-sm py-2 pl-2 md:pl-4">{tableInfo.title}</p>
      </div>
      <div className="mt-2">
        {tableInfo.contents.map((content, index) => (
          <div className="mb-0 px-2 md:mb-2 md:ml-4" key={index}>
            <p className="md:w-3/12 text-xs text-gray-400 md:inline-block">
              {content.itemNm}
            </p>
            {content.type === 'text' ? (
              <span className="pl-2 md:pl-0 text-xs text-gray-400 inline-block md:inline truncate">
                {content.value}
              </span>
            ) : (
              <a
                className="text-xs underline text-cyan-400 hover:text-cyan-600 cursor-pointer"
                onClick={content.onClick}
              >
                <span className="w-full pl-2 md:pl-0 inline-block md:inline truncate">
                  {content.value}
                </span>
              </a>
            )}
          </div>
        ))}
      </div>
    </div>
  )
}

export default Table
