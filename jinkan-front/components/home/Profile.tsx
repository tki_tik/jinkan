import Image from 'next/image'
import { calcAge, formatDate } from '../../lib/DateUtil'
import ProfileInfo from '../../types/ProfileInfo'

const Profile: React.FC<ProfileInfo> = (info) => {
  return (
    <div className="flex flex-col w-11/12 justify-center mx-auto pb-2">
      <div className="relative h-20 w-20 md:h-32 md:w-32">
        <Image
          className="rounded"
          src={info.profileImageUrl ? info.profileImageUrl : '/img/profile.svg'}
          layout="fill"
        />
      </div>
      <div className="relative -top-20 -mb-20 pl-24 md:-top-32 md:-mb-32 md:pl-40">
        <p className="text-xs text-gray-400">{`社員番号：${info.userId}`}</p>
        <p className="mt-3 md:mt-5 truncate">{info.userName}</p>
        <p className="text-xs text-gray-400 hidden md:block mt-1 md:mt-5">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5 inline-block mr-1 text-left"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z"
            />
          </svg>
          <span className="align-bottom">
            {info.sex === '1' ? '男性' : '女性'}
          </span>
        </p>
        <p className="text-xs text-gray-400 mt-3 md:mt-2">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5 hidden md:inline-block md:mr-1"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M21 15.546c-.523 0-1.046.151-1.5.454a2.704 2.704 0 01-3 0 2.704 2.704 0 00-3 0 2.704 2.704 0 01-3 0 2.704 2.704 0 00-3 0 2.704 2.704 0 01-3 0 2.701 2.701 0 00-1.5-.454M9 6v2m3-2v2m3-2v2M9 3h.01M12 3h.01M15 3h.01M21 21v-7a2 2 0 00-2-2H5a2 2 0 00-2 2v7h18zm-3-9v-2a2 2 0 00-2-2H8a2 2 0 00-2 2v2h12z"
            />
          </svg>
          <span className="align-bottom">{`${formatDate(
            info.birth,
            'YYYY/MM/DD'
          )} (${calcAge(info.birth)}歳)`}</span>
        </p>
      </div>
    </div>
  )
}

export default Profile
