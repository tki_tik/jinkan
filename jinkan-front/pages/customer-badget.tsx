import { GetStaticProps } from 'next'
import { useLocale } from '../hook/useLocale'
import GetMenuAuth from '../fetch/GetMenuAuth'

const CustomerBadget: React.FC = () => {
  const { t } = useLocale()

  return (
    <div className="flex justify-center items-center flex-col h-full">
      {t.common.preparing}
    </div>
  )
}

export default CustomerBadget

export const getStaticProps: GetStaticProps = async () => {
  const titleTag = 'customer'
  const authList: Array<string> = await GetMenuAuth(titleTag)
  return {
    props: {
      layout: true,
      title: titleTag,
      authList: authList,
    },
  }
}
