import { useState } from 'react'
import { GetStaticProps } from 'next'
import Tab from '../components/common/Tab'
import OtherTab from '../components/settings/OtherTab'
import ProfileTab from '../components/settings/ProfileTab'
import GetMenuAuth from '../fetch/GetMenuAuth'
import { useLocale } from '../hook/useLocale'
import TabInfo from '../types/TabInfo'

const Settings: React.FC = () => {
  const { t } = useLocale()
  const [activeTab, setActiveTab] = useState(0)

  const tabInfo: TabInfo = {
    tabList: [t.settings.tabTitle1, t.settings.tabTitle2],
    activeTab: activeTab,
    setActiveTab: setActiveTab,
  }

  return (
    <div className="flex justify-center h-full">
      <div className="w-3/4">
        <Tab {...tabInfo} />
        <div className="pb-8">
          <div className="w-full bg-white text-center mx-auto shadow-md rounded-lg py-5">
            <div className="flex flex-col max-w-sm justify-center mx-auto px-5">
              {activeTab === 0 && <ProfileTab />}
              {activeTab === 1 && <OtherTab />}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Settings

export const getStaticProps: GetStaticProps = async () => {
  const titleTag = 'setting'
  const authList: Array<string> = await GetMenuAuth(titleTag)
  return {
    props: {
      layout: true,
      title: titleTag,
      authList: authList,
    },
  }
}
