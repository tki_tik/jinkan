import { useRouter } from 'next/router'
import { useEffect } from 'react'
import Login from '../fetch/Login'
import Loading from '../components/layout/Loading'

const DoLogin: React.FC = () => {
  const router = useRouter()

  const login = async () => {
    const param = new URL(location.href).searchParams
    const result = await Login(param.get('code'))
    if (result === 'success') {
      router.push('/')
    }
  }

  useEffect(() => {
    login()
  }, [])

  return <Loading />
}

export default DoLogin
