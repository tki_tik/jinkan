import 'tailwindcss/tailwind.css'
import { AppProps } from 'next/app'
import { ToastProvider } from 'react-toast-notifications'
import Layout from '../components/layout/Layout'
import { StateProvider } from '../context/StateProvider'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      {pageProps.layout ? (
        <ToastProvider>
          <StateProvider>
            <Layout {...pageProps}>
              <Component {...pageProps} />
            </Layout>
          </StateProvider>
        </ToastProvider>
      ) : (
        <Component {...pageProps} />
      )}
    </>
  )
}

export default MyApp
