import { GetStaticProps } from 'next'
import { useEffect, useState } from 'react'
import Table from '../components/common/Table'
import Profile from '../components/home/Profile'
import { useStateContext } from '../context/StateProvider'
import GetMenuAuth from '../fetch/GetMenuAuth'
import { toDate } from '../lib/DateUtil'
import { getUserId, getUsername } from '../lib/GetAuthInfo'
import ProfileInfo from '../types/ProfileInfo'
import TableInfo from '../types/TableInfo'

const Home: React.FC = () => {
  const { tempUserInfo } = useStateContext()
  const [profileInfo, setProfileInfo] = useState({} as ProfileInfo)

  const tableInfoList: Array<TableInfo> = [
    {
      title: '従業員情報',
      contents: [
        {
          type: 'text',
          itemNm: '所属事業所',
          value: 'SYS関東事業部',
          onClick: null,
        },
        {
          type: 'text',
          itemNm: '所属部署',
          value: 'SEシステム１部',
          onClick: null,
        },
        {
          type: 'text',
          itemNm: '役職',
          value: 'サブリーダー',
          onClick: null,
        },
        {
          type: 'text',
          itemNm: '入社年月日',
          value: '2017/05/01 (4年2ヶ月)',
          onClick: null,
        },
        {
          type: 'text',
          itemNm: '住所',
          value: `${tempUserInfo.state} ${tempUserInfo.city}`,
          onClick: null,
        },
        {
          type: 'text',
          itemNm: '最寄駅',
          value: `${tempUserInfo.station}`,
          onClick: null,
        },
      ],
    },
    {
      title: '業務経歴',
      contents: [
        {
          type: 'link',
          itemNm: '業務経歴',
          value: '表示する',
          onClick: () => alert('業務経歴書　表示'),
        },
        {
          type: !tempUserInfo.portfolioUrl ? 'text' : 'link',
          itemNm: 'ポートフォリオURL',
          value: !tempUserInfo.portfolioUrl
            ? '未設定'
            : tempUserInfo.portfolioUrl,
          onClick: () => window.open(tempUserInfo.portfolioUrl, '_blank'),
        },
      ],
    },
    {
      title: 'ニックネーム',
      contents: [
        {
          type: 'text',
          itemNm: 'ニックネーム',
          value: tempUserInfo.dispNm,
          onClick: null,
        },
      ],
    },
  ]

  useEffect(() => {
    const profileInfoStub: ProfileInfo = {
      profileImageUrl: tempUserInfo.profileImageUrl,
      userId: getUserId(),
      userName: getUsername(),
      sex: '1',
      birth: toDate('1991-02-08', 'YYYY-MM-DD'),
      profileDetail: {
        employeeInfo: {
          office: 'SYS関東事業本部',
          department: '3SEシステム１部',
          role: 'サブリーダー',
          joinDate: toDate('2017-05-01', 'YYYY-MM-DD'),
          address: `${tempUserInfo.state} ${tempUserInfo.city}`,
          station: tempUserInfo.station,
        },
        portfolioUrl: tempUserInfo.portfolioUrl,
        nickName: tempUserInfo.dispNm,
      },
    }
    setProfileInfo(profileInfoStub)
  }, [tempUserInfo])

  return (
    <div className="w-3/4 bg-white my-8 mx-auto py-5 shadow-md rounded-lg">
      <Profile {...profileInfo} />
      {tableInfoList.map((info, index) => (
        <Table {...info} key={index} />
      ))}
    </div>
  )
}

export default Home

export const getStaticProps: GetStaticProps = async () => {
  const titleTag = 'home'
  const authList: Array<string> = await GetMenuAuth(titleTag)
  return {
    props: {
      layout: true,
      title: titleTag,
      authList: authList,
    },
  }
}
