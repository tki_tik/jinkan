import { GetStaticProps } from 'next'
import GetMenuAuth from '../fetch/GetMenuAuth'
import { useLocale } from '../hook/useLocale'

const ImportantEngineerList: React.FC = () => {
  const { t } = useLocale()

  return (
    <div className="flex justify-center items-center flex-col h-full">
      {t.common.preparing}
    </div>
  )
}

export default ImportantEngineerList

export const getStaticProps: GetStaticProps = async () => {
  const titleTag = 'engineer'
  const authList: Array<string> = await GetMenuAuth(titleTag)
  return {
    props: {
      layout: true,
      title: titleTag,
      authList: authList,
    },
  }
}
