import DefaultErrorPage from 'next/error'
import Head from 'next/head'
import { useRouter } from 'next/router'

const Error: React.FC = () => {
  const router = useRouter()
  const code = router.query.code
  let title = router.query.title

  if (Array.isArray(title)) {
    title = title.join()
  }

  return (
    <>
      <Head>
        <title>エラー</title>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="author" content="tki_tik" />
      </Head>
      <DefaultErrorPage statusCode={Number(code)} title={title} />
    </>
  )
}

export default Error
