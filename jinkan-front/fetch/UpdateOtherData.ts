import { callApi, httpClient, Response } from '../lib/CallApi'
import OtherSettingInfo from '../types/OtherSettingInfo'

const UpdateOtherData = async (
  token: string,
  info: OtherSettingInfo,
  setIsLoading: React.Dispatch<React.SetStateAction<boolean>>
): Promise<Response> => {
  const req = () => {
    return httpClient(token, 'application/json').post(
      `${process.env.NEXT_PUBLIC_JINKAN_RESTAPI_URL}/api/update-other-data`,
      info
    )
  }
  const res = await callApi<void>(req, null, setIsLoading)
  return res
}

export default UpdateOtherData
