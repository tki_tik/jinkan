import { callApi, httpClient } from '../lib/CallApi'

const Logout = async (token: string): Promise<void> => {
  const req = () => {
    return httpClient(token, '').post(
      `${process.env.NEXT_PUBLIC_JINKAN_RESTAPI_URL}/auth/logout`
    )
  }
  await callApi<void>(req, null, null)
}

export default Logout
