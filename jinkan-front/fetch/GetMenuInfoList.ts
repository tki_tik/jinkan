import { callApi, httpClient, Response } from '../lib/CallApi'
import { getRole } from '../lib/GetAuthInfo'
import MenuInfoResponse from '../types/MenuInfoResponse'

const GetMenuInfoList = async (
  token: string,
  setIsLoading: React.Dispatch<React.SetStateAction<boolean>>
): Promise<Response> => {
  const req = () => {
    return httpClient(token, 'application/json').get(
      `${
        process.env.NEXT_PUBLIC_JINKAN_RESTAPI_URL
      }/api/get-menu-info?auth=${getRole()}`
    )
  }
  const res = await callApi<MenuInfoResponse>(
    req,
    { menuInfoList: [] },
    setIsLoading
  )
  return res
}

export default GetMenuInfoList
