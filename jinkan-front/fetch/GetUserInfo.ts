import { callApi, httpClient, Response } from '../lib/CallApi'
import { getUserId } from '../lib/GetAuthInfo'
import UserInfo from '../types/UserInfo'

const GetUserInfo = async (
  token: string,
  setIsLoading: React.Dispatch<React.SetStateAction<boolean>>
): Promise<Response> => {
  const req = () => {
    return httpClient(token, 'application/json').get(
      `${
        process.env.NEXT_PUBLIC_JINKAN_RESTAPI_URL
      }/api/get-user-info?userId=${getUserId()}`
    )
  }
  const initialData = {
    dispNm: null,
    profileImageUrl: null,
    state: '',
    city: '',
    station: '',
    locale: '',
    searchTag: null,
    portfolioUrl: null,
  }
  const res = await callApi<UserInfo>(req, initialData, setIsLoading)
  return res
}

export default GetUserInfo
