import { callApi, httpClient } from '../lib/CallApi'

const Login = async (code: string | string[]): Promise<'success' | 'error'> => {
  const req = () => {
    return httpClient(null, 'application/json').get(
      `${process.env.NEXT_PUBLIC_JINKAN_RESTAPI_URL}/auth/login?code=${code}`
    )
  }
  const res = await callApi<void>(req, null, null)
  return res.status
}

export default Login
