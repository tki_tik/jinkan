import axios from 'axios'

const GetMenuAuth = async (menuTitleTag: string): Promise<Array<string>> => {
  const res = await axios.get(
    `${process.env.NEXT_PUBLIC_JINKAN_RESTAPI_URL}/api/get-menu-auth`,
    {
      params: {
        menuTitleTag: menuTitleTag,
      },
    }
  )
  return res.data.authList
}

export default GetMenuAuth
