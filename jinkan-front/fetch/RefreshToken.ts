import { callApi, httpClient } from '../lib/CallApi'

const RefreshToken = async (token: string): Promise<void> => {
  const req = () => {
    return httpClient(token, '').post(
      `${process.env.NEXT_PUBLIC_JINKAN_RESTAPI_URL}/api/refresh-token`
    )
  }
  await callApi<void>(req, null, null)
}

export default RefreshToken
