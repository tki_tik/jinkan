import { callApi, httpClient, Response } from '../lib/CallApi'
import ProfileSettingInfo from '../types/ProfileSettingInfo'

const UpdateProfileData = async (
  token: string,
  info: ProfileSettingInfo,
  setIsLoading: React.Dispatch<React.SetStateAction<boolean>>
): Promise<Response> => {
  const req = () => {
    return httpClient(token, 'application/json').post(
      `${process.env.NEXT_PUBLIC_JINKAN_RESTAPI_URL}/api/update-profile-data`,
      info
    )
  }
  const res = await callApi<void>(req, null, setIsLoading)
  return res
}

export default UpdateProfileData
