import { FileUpload } from 'use-file-upload'
import { callApi, httpClient, Response } from '../lib/CallApi'
import { getUserId } from '../lib/GetAuthInfo'

const UploadProfileImage = async (
  token: string,
  image: FileUpload,
  setIsLoading: React.Dispatch<React.SetStateAction<boolean>>
): Promise<Response> => {
  let formData = new FormData()
  formData.append('userId', getUserId())
  if (image) {
    formData.append('profileImage', image.file)
  }
  const req = () => {
    return httpClient(token, 'multipart/form-data').post(
      `${process.env.NEXT_PUBLIC_JINKAN_RESTAPI_URL}/api/upload-profile-image`,
      formData
    )
  }
  const res = await callApi<string>(req, '', setIsLoading)
  return res
}

export default UploadProfileImage
