export default interface OtherSettingInfo {
  userId: string
  locale: string
  searchTag: string
  portfolioUrl: string
}
