export default interface UserInfo {
  dispNm: string
  profileImageUrl: string
  state: string
  city: string
  station: string
  locale: string
  searchTag: string
  portfolioUrl: string
}
