interface EmployeeInfo {
  office: string
  department: string
  role: string
  joinDate: Date
  address: string
  station: string
}

interface ProfileDetail {
  employeeInfo: EmployeeInfo
  portfolioUrl: string
  nickName: string
}

export default interface ProfileInfo {
  profileImageUrl: string
  userId: string
  userName: string
  sex: '1' | '2'
  birth: Date
  profileDetail: ProfileDetail
}
