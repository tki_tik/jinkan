export default interface AuthInfo {
  exp: number
  iat: number
  role: string
  sub: string
  username: string
}
