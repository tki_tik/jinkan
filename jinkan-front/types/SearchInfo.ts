export default interface SearchInfo {
  value: string
  setValue: React.Dispatch<React.SetStateAction<string>>
  placeholder: string
  onClick: () => any
}
