export default interface ProfileSettingInfo {
  userId: string
  dispNm: string
  state: string
  city: string
  station: string
}
