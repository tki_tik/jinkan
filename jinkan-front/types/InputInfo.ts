export default interface InputInfo {
  id: string
  type: string
  itemNm: string
  value: string
  setValue: React.Dispatch<React.SetStateAction<string>>
  isRequired: boolean
}
