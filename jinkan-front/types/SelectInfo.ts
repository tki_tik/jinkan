export default interface SelectInfo {
  name: string
  itemNm: string
  value: string
  setValue: React.Dispatch<React.SetStateAction<string>>
  optionList: Array<Option>
}

interface Option {
  name: string
  value: string
}
