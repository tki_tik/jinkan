export default interface TabInfo {
  tabList: Array<string>
  activeTab: number
  setActiveTab: React.Dispatch<React.SetStateAction<number>>
}
