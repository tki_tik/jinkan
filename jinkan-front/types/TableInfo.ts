interface TableContent {
  type: 'text' | 'link'
  itemNm: string
  value: string
  onClick: () => any | null
}

export default interface TableInfo {
  title: string
  contents: Array<TableContent>
}
