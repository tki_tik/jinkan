import React, { useContext, useState, createContext, useEffect } from 'react'
import UserInfo from '../types/UserInfo'

const StateContext = createContext(
  {} as {
    tempUserInfo: UserInfo
    setTempUserInfo: React.Dispatch<React.SetStateAction<UserInfo>>
    userInfo: UserInfo
    setUserInfo: React.Dispatch<React.SetStateAction<UserInfo>>
    dispNm: string
    setDispNm: React.Dispatch<React.SetStateAction<string>>
    profileImageUrl: string
    setProfileImageUrl: React.Dispatch<React.SetStateAction<string>>
    state: string
    setState: React.Dispatch<React.SetStateAction<string>>
    city: string
    setCity: React.Dispatch<React.SetStateAction<string>>
    station: string
    setStation: React.Dispatch<React.SetStateAction<string>>
    locale: string
    setLocale: React.Dispatch<React.SetStateAction<string>>
    searchTag: string
    setSearchTag: React.Dispatch<React.SetStateAction<string>>
    portfolioUrl: string
    setPortfolioUrl: React.Dispatch<React.SetStateAction<string>>
    initializelUserInfo: () => void
    isLoading: boolean
    setIsLoading: React.Dispatch<React.SetStateAction<boolean>>
  }
)

export const StateProvider: React.FC = ({ children }) => {
  const [tempUserInfo, setTempUserInfo] = useState({
    dispNm: null,
    profileImageUrl: null,
    state: '',
    city: '',
    station: '',
    locale: '',
    searchTag: null,
    portfolioUrl: null,
  })
  const [userInfo, setUserInfo] = useState({
    dispNm: null,
    profileImageUrl: null,
    state: '',
    city: '',
    station: '',
    locale: '',
    searchTag: null,
    portfolioUrl: null,
  })
  const [dispNm, setDispNm] = useState('')
  const [profileImageUrl, setProfileImageUrl] = useState('')
  const [state, setState] = useState('')
  const [city, setCity] = useState('')
  const [station, setStation] = useState('')
  const [locale, setLocale] = useState('')
  const [searchTag, setSearchTag] = useState('')
  const [portfolioUrl, setPortfolioUrl] = useState('')
  const [isLoading, setIsLoading] = useState(false)

  const initializelUserInfo = () => {
    setDispNm(tempUserInfo.dispNm)
    setState(tempUserInfo.state)
    setCity(tempUserInfo.city)
    setStation(tempUserInfo.station)
    setLocale(tempUserInfo.locale)
    setSearchTag(tempUserInfo.searchTag)
    setPortfolioUrl(tempUserInfo.portfolioUrl)
  }

  useEffect(() => {
    setDispNm(userInfo.dispNm)
    setProfileImageUrl(userInfo.profileImageUrl)
    setState(userInfo.state)
    setCity(userInfo.city)
    setStation(userInfo.station)
    setLocale(userInfo.locale)
    setSearchTag(userInfo.searchTag)
    setPortfolioUrl(userInfo.portfolioUrl)
  }, [userInfo])

  return (
    <StateContext.Provider
      value={{
        tempUserInfo,
        setTempUserInfo,
        userInfo,
        setUserInfo,
        dispNm,
        setDispNm,
        profileImageUrl,
        setProfileImageUrl,
        state,
        setState,
        city,
        setCity,
        station,
        setStation,
        locale,
        setLocale,
        searchTag,
        setSearchTag,
        portfolioUrl,
        setPortfolioUrl,
        initializelUserInfo,
        isLoading,
        setIsLoading,
      }}
    >
      {children}
    </StateContext.Provider>
  )
}

export const useStateContext = () => useContext(StateContext)
