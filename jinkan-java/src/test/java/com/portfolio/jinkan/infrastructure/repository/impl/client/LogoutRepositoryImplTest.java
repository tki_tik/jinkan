package com.portfolio.jinkan.infrastructure.repository.impl.client;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.portfolio.jinkan.infrastructure.client.authapi.LogoutClient;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class LogoutRepositoryImplTest {

    @InjectMocks
    private LogoutRepositoryImpl target;

    @Mock
    private LogoutClient client;

    @Test
    @DisplayName("ログアウト処理を呼び出すこと")
    void logoutTest() {

        // setup
        doNothing().when(this.client).logout();

        // execute
        this.target.logout();

        // assert
        verify(this.client, times(1)).logout();
    }
}
