package com.portfolio.jinkan.infrastructure.repository.impl;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import javax.transaction.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.portfolio.jinkan.domain.model.MenuInfoModel;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

@SpringBootTest
@TestExecutionListeners({
    DependencyInjectionTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class
})
@Transactional
public class MstMenuRepositoryImplTest {

    @Autowired
    private MstMenuRepositoryImpl target;

    @Test
    @DatabaseSetup("/dbunit/mstmenu.xml")
    @DisplayName("メニュータイトルを条件に権限リストが正常に取得できること")
    void getAuthorityListTest() {

        // setup
        String title = "home";
        String expectedAuth1 = "ROLE_ADMIN";
        String expectedAuth2 = "ROLE_USER";

        // execute
        List<String> actualList = this.target.getAuthorityList(title);

        // assert
        assertAll(() -> {
            assertEquals(2, actualList.size());
        }, () -> {
            assertEquals(expectedAuth1, actualList.get(0));
        }, () -> {
            assertEquals(expectedAuth2, actualList.get(1));
        });
    }

    @Test
    @DatabaseSetup("/dbunit/mstmenu.xml")
    @DisplayName("権限を条件に表示対象のメニュー情報が正常に取得できること")
    void getMenuInfoListTest() {

        // setup
        String auth = "ROLE_USER";
        String expectedId1 = "001";
        String expectedUrl1 = "/";
        String expectedPath1 = "M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6";
        String expectedTitleTag1 = "home";
        String expectedId2 = "002";
        String expectedUrl2 = "/employee-search";
        String expectedPath2 = "M8 16l2.879-2.879m0 0a3 3 0 104.243-4.242 3 3 0 00-4.243 4.242zM21 12a9 9 0 11-18 0 9 9 0 0118 0z";
        String expectedTitleTag2 = "employee";

        // execute
        List<MenuInfoModel> actualList = this.target.getMenuInfoList(auth);

        // assert
        assertAll(() -> {
            assertEquals(2, actualList.size());
        }, () -> {
            assertEquals(expectedId1, actualList.get(0).getId());
        }, () -> {
            assertEquals(expectedUrl1, actualList.get(0).getUrl());
        }, () -> {
            assertEquals(expectedPath1, actualList.get(0).getPath());
        }, () -> {
            assertEquals(expectedTitleTag1, actualList.get(0).getTitleTag());
        }, () -> {
            assertEquals(expectedId2, actualList.get(1).getId());
        }, () -> {
            assertEquals(expectedUrl2, actualList.get(1).getUrl());
        }, () -> {
            assertEquals(expectedPath2, actualList.get(1).getPath());
        }, () -> {
            assertEquals(expectedTitleTag2, actualList.get(1).getTitleTag());
        });
    }
}
