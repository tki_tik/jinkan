package com.portfolio.jinkan.infrastructure.repository.impl.client;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.UUID;

import com.portfolio.jinkan.domain.model.AuthInfoModel;
import com.portfolio.jinkan.infrastructure.client.authapi.GetAuthInfoClient;
import com.portfolio.jinkan.infrastructure.dto.authapi.AuthInfo;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class GetAuthInfoRepositoryImplTest {

    @InjectMocks
    private GetAuthInfoRepositoryImpl target;

    @Mock
    private GetAuthInfoClient client;

    @Test
    @DisplayName("認可コードに紐づく認証情報を正常に取得できること")
    void getAuthInfoTest() {

        // setup
        String code = UUID.randomUUID().toString();
        String userId = "999999";
        String userName = "admin";
        String role = "ROLE_ADMIN";
        AuthInfo authInfo = AuthInfo.builder().userId(userId).userName(userName).role(role).build();
        doReturn(authInfo).when(this.client).getAuthInfo(code);

        // execute
        AuthInfoModel actual = this.target.getAuthInfo(code);

        // assert
        assertAll(() -> {
            assertEquals(userId, actual.getUserId());
        }, () -> {
            assertEquals(userName, actual.getUserName());
        }, () -> {
            assertEquals(role, actual.getRole());
        }, () -> {
            verify(this.client, times(1)).getAuthInfo(code);
        });
    }
}
