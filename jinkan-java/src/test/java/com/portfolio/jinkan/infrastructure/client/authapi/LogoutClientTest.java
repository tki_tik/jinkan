package com.portfolio.jinkan.infrastructure.client.authapi;

import com.portfolio.jinkan.common.WiremockTestUtil;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class LogoutClientTest {

    @Autowired
    private WiremockTestUtil wiremock;

    @Autowired
    private LogoutClient target;

    @BeforeEach
    void setup() {
        this.wiremock.start();
    }

    @AfterEach
    void tearDown() {
        this.wiremock.stop();
    }

    @Test
    @DisplayName("正常にログアウトAPIを呼び出すこと")
    void logoutTest() {

        // execute
        this.target.logout();
    }
}
