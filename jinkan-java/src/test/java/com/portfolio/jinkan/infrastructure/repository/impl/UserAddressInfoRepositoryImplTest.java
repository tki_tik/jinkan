package com.portfolio.jinkan.infrastructure.repository.impl;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.transaction.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.portfolio.jinkan.domain.model.UpdateProfileDataModel;
import com.portfolio.jinkan.domain.model.UserAddressInfoModel;
import com.portfolio.jinkan.infrastructure.entity.UserAddressInfoEntity;
import com.portfolio.jinkan.infrastructure.repository.jpa.UserAddressInfoJpaRepository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

@SpringBootTest
@TestExecutionListeners({
    DependencyInjectionTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class
})
@Transactional
public class UserAddressInfoRepositoryImplTest {

    @Autowired
    private UserAddressInfoRepositoryImpl target;

    @Autowired
    private UserAddressInfoJpaRepository jpaRepository;

    @Test
    @DatabaseSetup("/dbunit/useraddressinfo.xml")
    @DisplayName("ユーザIDを条件にユーザ住所情報が正常に取得できること")
    void getUserAddressInfoTest() {

        // setup
        String userId = "999999";
        String expectedState = "東京都";
        String expectedCity = "中央区";
        String expectedStation = "人形町駅";

        // execute
        UserAddressInfoModel actual = this.target.getUserAddressInfo(userId);

        // assert
        assertAll(() -> {
            assertEquals(expectedState, actual.getState());
        }, () -> {
            assertEquals(expectedCity, actual.getCity());
        }, () -> {
            assertEquals(expectedStation, actual.getStation());
        });
    }

    @Test
    @DatabaseSetup("/dbunit/useraddressinfo.xml")
    @DisplayName("ユーザIDに紐づいたユーザ住所情報が存在しない場合、RunTimeExceptionスローすること")
    void getUserAddressInfoNoneTest() {

        // setup
        String userId = "000000";

        // execute & assert
        assertThrows(RuntimeException.class, () -> {
            this.target.getUserAddressInfo(userId);
        });
    }

    @Test
    @DatabaseSetup("/dbunit/useraddressinfo.xml")
    @DisplayName("引数のユーザ住所情報を元にユーザ住所情報を更新すること")
    void updateTest() throws ParseException {

        // setup
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        String userId = "000001";
        UpdateProfileDataModel model = UpdateProfileDataModel.builder()
            .userId(userId)
            .dispNm("ユーザ")
            .state("東京都")
            .city("中央区")
            .station("人形町駅")
            .build();
        String expectedState = "東京都";
        String expectedCity = "中央区";
        String expectedStation = "人形町駅";
        String expectedCreatedUserId = "999999";
        Date expectedDate = sdf.parse("2021-05-21 11:47:41.0");

        // execute
        int actual = this.target.update(model);

        // assert
        UserAddressInfoEntity entity = this.jpaRepository.findById(userId).orElse(new UserAddressInfoEntity());
        assertAll(() -> {
            assertEquals(1, actual);
        }, () -> {
            assertEquals(expectedState, entity.getState());
        }, () -> {
            assertEquals(expectedCity, entity.getCity());
        }, () -> {
            assertEquals(expectedStation, entity.getStation());
        }, () -> {
            assertEquals(expectedCreatedUserId, entity.getCreatedUserId());
        }, () -> {
            assertEquals(expectedDate, entity.getCreatedAt());
        }, () -> {
            assertEquals(userId, entity.getUpdatedUserId());
        }, () -> {
            assertNotEquals(expectedDate, entity.getUpdatedAt());
        });
    }
}
