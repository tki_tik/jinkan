package com.portfolio.jinkan.infrastructure.repository.impl;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.transaction.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.portfolio.jinkan.domain.model.UpdateOtherDataModel;
import com.portfolio.jinkan.domain.model.UserSettingInfoModel;
import com.portfolio.jinkan.infrastructure.entity.UserSettingInfoEntity;
import com.portfolio.jinkan.infrastructure.repository.jpa.UserSettingInfoJpaRepository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

@SpringBootTest
@TestExecutionListeners({
    DependencyInjectionTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class
})
@Transactional
public class UserSettingInfoRepositoryImplTest {

    @Autowired
    private UserSettingInfoRepositoryImpl target;

    @Autowired
    private UserSettingInfoJpaRepository jpaRepository;

    @Test
    @DatabaseSetup("/dbunit/usersettinginfo.xml")
    @DisplayName("ユーザIDを条件にユーザ設定情報が正常に取得できること")
    void getUserSettingInfoTest() {

        // setup
        String userId = "999999";
        String expectedLocale = "ja";
        String expectedSearchTag = "java Spring Boot";
        String expectedPortfolioUrl = "https://gitlab.com/tki_tik/auth-api-java";

        // execute
        UserSettingInfoModel actual = this.target.getUserSettingInfo(userId);

        // assert
        assertAll(() -> {
            assertEquals(expectedLocale, actual.getLocale());
        }, () -> {
            assertEquals(expectedSearchTag, actual.getSearchTag());
        }, () -> {
            assertEquals(expectedPortfolioUrl, actual.getPortfolioUrl());
        });
    }

    @Test
    @DatabaseSetup("/dbunit/usersettinginfo.xml")
    @DisplayName("ユーザIDに紐づいたユーザ設定情報が存在しない場合、RunTimeExceptionスローすること")
    void getUserSettingInfoNoneTest() {

        // setup
        String userId = "000000";

        // execute & assert
        assertThrows(RuntimeException.class, () -> {
            this.target.getUserSettingInfo(userId);
        });
    }

    @Test
    @DatabaseSetup("/dbunit/usersettinginfo.xml")
    @DisplayName("引数のユーザ設定情報を元にユーザ設定情報を更新すること")
    void updateTest() throws ParseException {

        // setup
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        String userId = "000001";
        UpdateOtherDataModel model = UpdateOtherDataModel.builder()
            .userId(userId)
            .locale("zh")
            .searchTag("python")
            .portfolioUrl("https://gitlab.com/test")
            .build();
        String expectedLocale = "zh";
        String expectedSearchTag = "python";
        String expectedPortfolioUrl = "https://gitlab.com/test";
        String expectedCreatedUserId = "999999";
        Date expectedDate = sdf.parse("2021-05-21 11:47:41.0");

        // execute
        int actual = this.target.update(model);

        // assert
        UserSettingInfoEntity entity = this.jpaRepository.findById(userId).orElse(new UserSettingInfoEntity());
        assertAll(() -> {
            assertEquals(1, actual);
        }, () -> {
            assertEquals(expectedLocale, entity.getLocale());
        }, () -> {
            assertEquals(expectedSearchTag, entity.getSearchTag());
        }, () -> {
            assertEquals(expectedPortfolioUrl, entity.getPortfolioUrl());
        }, () -> {
            assertEquals(expectedCreatedUserId, entity.getCreatedUserId());
        }, () -> {
            assertEquals(expectedDate, entity.getCreatedAt());
        }, () -> {
            assertEquals(userId, entity.getUpdatedUserId());
        }, () -> {
            assertNotEquals(expectedDate, entity.getUpdatedAt());
        });
    }
}
