package com.portfolio.jinkan.infrastructure.client.authapi;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.portfolio.jinkan.common.WiremockTestUtil;
import com.portfolio.jinkan.infrastructure.dto.authapi.AuthInfo;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class GetAuthInfoClientTest {

    @Autowired
    private WiremockTestUtil wiremock;

    @Autowired
    private GetAuthInfoClient target;

    @BeforeEach
    void setup() {
        this.wiremock.start();
    }

    @AfterEach
    void tearDown() {
        this.wiremock.stop();
    }

    @Test
    @DisplayName("認可コードを認証情報取得APIを呼び出し認証方法を正常に取得できること")
    void Test() {

        // setup
        String code = "test-auth-code";
        String expectedUserId = "999999";
        String expectedUserName = "admin";
        String expectedRole = "ROLE_ADMIN";

        // execute
        AuthInfo actual = this.target.getAuthInfo(code);

        // assert
        assertAll(() -> {
            assertEquals(expectedUserId, actual.getUserId());
        }, () -> {
            assertEquals(expectedUserName, actual.getUserName());
        }, () -> {
            assertEquals(expectedRole, actual.getRole());
        });
    }
}
