package com.portfolio.jinkan.infrastructure.repository.impl;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.FileInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import javax.transaction.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.portfolio.jinkan.domain.model.UserInfoModel;
import com.portfolio.jinkan.infrastructure.entity.UserInfoEntity;
import com.portfolio.jinkan.infrastructure.repository.jpa.UserInfoJpaRepository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

@SpringBootTest
@TestExecutionListeners({
    DependencyInjectionTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class
})
@Transactional
public class UserInfoRepositoryImplTest {

    @Autowired
    private UserInfoRepositoryImpl target;

    @Autowired
    private UserInfoJpaRepository jpaRepository;

    @Test
    @DatabaseSetup("/dbunit/userinfo.xml")
    @DisplayName("ユーザIDを条件にユーザ情報が正常に取得できること")
    void getUserInfoTest() {

        // setup
        String userId = "999999";
        String expectedDispNm = "あどみん　次郎";
        String expectedProfileImage = Base64.getEncoder().encodeToString("test.jpg".getBytes());
        String expectedImageType = "image/jpeg";

        // execute
        UserInfoModel actual = this.target.getUserInfo(userId);

        // assert
        assertAll(() -> {
            assertEquals(expectedDispNm, actual.getDispNm());
        }, () -> {
            assertEquals(expectedProfileImage, Base64.getEncoder().encodeToString(actual.getProfileImage()));
        }, () -> {
            assertEquals(expectedImageType, actual.getImageType());
        });
    }

    @Test
    @DatabaseSetup("/dbunit/userinfo.xml")
    @DisplayName("ユーザIDに紐づいたユーザ情報が存在しない場合、RunTimeExceptionスローすること")
    void getUserInfoNoneTest() {

        // setup
        String userId = "000000";

        // execute & assert
        assertThrows(RuntimeException.class, () -> {
            this.target.getUserInfo(userId);
        });
    }

    @Test
    @DatabaseSetup("/dbunit/userinfo.xml")
    @DisplayName("ユーザIDを元にプロフィール画像及びプロフィール画像拡張子をnullに更新すること")
    void updateProfileImageNullTest() throws Exception {

        // setup
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        String userId = "000001";
        String expectedDispNm = "ゆーざ　三郎";
        String expectedCreatedUserId = "999999";
        Date expectedDate = sdf.parse("2021-05-21 11:47:41.0");

        // execute
        int actual = this.target.updateProfileImage(userId);

        // asserrt
        UserInfoEntity entity = this.jpaRepository.findById(userId).orElse(new UserInfoEntity());
        assertAll(() -> {
            assertEquals(1, actual);
        }, () -> {
            assertEquals(expectedDispNm, entity.getDispNm());
        }, () -> {
            assertNull(entity.getProfileImage());
        }, () -> {
            assertNull(entity.getImageType());
        }, () -> {
            assertEquals(expectedCreatedUserId, entity.getCreatedUserId());
        }, () -> {
            assertEquals(expectedDate, entity.getCreatedAt());
        }, () -> {
            assertEquals(userId, entity.getUpdatedUserId());
        }, () -> {
            assertNotEquals(expectedDate, entity.getUpdatedAt());
        });
    }

    @Test
    @DatabaseSetup("/dbunit/userinfo.xml")
    @DisplayName("ユーザIDを元にプロフィール画像及びプロフィール画像拡張子を更新すること")
    void updateProfileImageTest() throws Exception {

        // setup
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        String userId = "000001";
        FileInputStream inputFile = new FileInputStream("src/test/resources/file/test.jpg");
        MockMultipartFile profileImage =
            new MockMultipartFile("test.jpg", "test.jpg", MediaType.IMAGE_JPEG_VALUE, inputFile);
        String expectedDispNm = "ゆーざ　三郎";
        String expectedImageType = "image/jpeg";
        String expectedCreatedUserId = "999999";
        Date expectedDate = sdf.parse("2021-05-21 11:47:41.0");

        // execute
        int actual = this.target.updateProfileImage(userId, profileImage);

        // asserrt
        UserInfoEntity entity = this.jpaRepository.findById(userId).orElse(new UserInfoEntity());
        assertAll(() -> {
            assertEquals(1, actual);
        }, () -> {
            assertEquals(expectedDispNm, entity.getDispNm());
        }, () -> {
            assertNotNull(entity.getProfileImage());
        }, () -> {
            assertEquals(expectedImageType, entity.getImageType());
        }, () -> {
            assertEquals(expectedCreatedUserId, entity.getCreatedUserId());
        }, () -> {
            assertEquals(expectedDate, entity.getCreatedAt());
        }, () -> {
            assertEquals(userId, entity.getUpdatedUserId());
        }, () -> {
            assertNotEquals(expectedDate, entity.getUpdatedAt());
        });
    }

    @Test
    @DatabaseSetup("/dbunit/userinfo.xml")
    @DisplayName("ユーザIDを元に表示名を更新すること")
    void updateDispNmTest() throws ParseException {

        // setup
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        String userId = "000001";
        String dispNm = "ユーザ";
        String expectedDispNm = "ユーザ";
        String expectedCreatedUserId = "999999";
        Date expectedDate = sdf.parse("2021-05-21 11:47:41.0");

        // execute
        int actual = this.target.updateDispNm(userId, dispNm);

        // asserrt
        UserInfoEntity entity = this.jpaRepository.findById(userId).orElse(new UserInfoEntity());
        assertAll(() -> {
            assertEquals(1, actual);
        }, () -> {
            assertEquals(expectedDispNm, entity.getDispNm());
        }, () -> {
            assertNull(entity.getProfileImage());
        }, () -> {
            assertNull(entity.getImageType());
        }, () -> {
            assertEquals(expectedCreatedUserId, entity.getCreatedUserId());
        }, () -> {
            assertEquals(expectedDate, entity.getCreatedAt());
        }, () -> {
            assertEquals(userId, entity.getUpdatedUserId());
        }, () -> {
            assertNotEquals(expectedDate, entity.getUpdatedAt());
        });
    }
}
