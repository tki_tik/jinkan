package com.portfolio.jinkan.common.interceptor;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;

import javax.servlet.http.Cookie;

import com.portfolio.jinkan.common.JWTProvider;
import com.portfolio.jinkan.common.Utility;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import io.undertow.util.Methods;

@SpringBootTest
public class JWTAuthenticationHandlerInterceptorTest {

    @InjectMocks
    private JWTAuthenticationHandlerInterceptor target;

    @Mock
    private JWTProvider provider;

    @Mock
    private Utility utility;

    @Test
    @DisplayName("JWT有効期限内：HTTPステータス200を設定、JWTProvider.setTokenメソッドを呼び出し、trueを返却")
    void preHandleNonExpiredTest() throws IOException {

        // setup
        Logger logger = (Logger) LoggerFactory.getLogger(JWTAuthenticationHandlerInterceptor.class);
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        String requestURI = "/api/test";
        String jwt2121 = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5OTk5OTkiLCJ1c2VybmFtZSI6ImFkbWluIiwicm9sZSI6IlJPTEVfQURNSU4iLCJpYXQiOjE2MjEwNjEwNzEsImV4cCI6NDc3NTA2MTA3MX0.5-SeAcp5dc17e2epjgb2C5vkWLTdFsSzk7p9h07qUs0";
        String jwt2126 = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5OTk5OTkiLCJ1c2VybmFtZSI6ImFkbWluIiwicm9sZSI6IlJPTEVfQURNSU4iLCJpYXQiOjE2MjE0NzM2MjksImV4cCI6NDkzNTAyMTYyOX0.uyxRx5MmyVw-VKfyjlVHCKlnRhHxAYPrG__z4X-nCeM";
        request.setRequestURI(requestURI);
        request.setCookies(new Cookie("token", jwt2121));
        String expectedLog1 = "【SERVICE-START】" + requestURI;
        String expectedLog2 = "【AUTHORIZE-START】" + requestURI;
        String expectedLog3 = "token : " + jwt2121;
        String expectedLog4 = "【AUTHORIZE-END】" + requestURI;
        ListAppender<ILoggingEvent> listAppender = new ListAppender<>();
        listAppender.start();
        logger.addAppender(listAppender);
        doReturn(jwt2121).when(this.provider).getToken(request);
        doReturn(true).when(this.provider).validateToken(jwt2121);
        doReturn(jwt2126).when(this.provider).refreshToken(jwt2121);
        doNothing().when(this.provider).setToken(response, jwt2126);

        // execute
        boolean actual = this.target.preHandle(request, response, new Object());
        listAppender.stop();

        // assert
        assertAll(() -> {
            assertEquals(HttpStatus.OK.value(), response.getStatus());
        }, () -> {
            assertTrue(actual);
        }, () -> {
            assertEquals(4, listAppender.list.size());
        }, () -> {
            assertEquals(expectedLog1, listAppender.list.get(0).getMessage());
        }, () -> {
            assertEquals(Level.INFO, listAppender.list.get(0).getLevel());
        }, () -> {
            assertEquals(expectedLog2, listAppender.list.get(1).getMessage());
        }, () -> {
            assertEquals(Level.INFO, listAppender.list.get(1).getLevel());
        }, () -> {
            assertEquals(expectedLog3, listAppender.list.get(2).getMessage());
        }, () -> {
            assertEquals(Level.INFO, listAppender.list.get(2).getLevel());
        }, () -> {
            assertEquals(expectedLog4, listAppender.list.get(3).getMessage());
        }, () -> {
            assertEquals(Level.INFO, listAppender.list.get(3).getLevel());
        }, () -> {
            verify(this.provider, times(1)).getToken(request);
        }, () -> {
            verify(this.provider, times(1)).validateToken(jwt2121);
        }, () -> {
            verify(this.provider, times(1)).refreshToken(jwt2121);
        }, () -> {
            verify(this.provider, times(1)).setToken(response, jwt2126);
        });
    }

    @Test
    @DisplayName("JWT有効期限切れ：HTTPステータス401を設定、falseを返却")
    void preHandleExpiredTest() throws IOException {

        // setup
        Logger logger = (Logger) LoggerFactory.getLogger(JWTAuthenticationHandlerInterceptor.class);
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        String requestURI = "/api/test";
        String jwtExpired = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5OTk5OTkiLCJ1c2VybmFtZSI6ImFkbWluIiwicm9sZSI6IlJPTEVfQURNSU4iLCJpYXQiOjE2MjE0NzY2MzksImV4cCI6MTYyMTQ3NjYzOX0.czjgYheB1UX2Ra0X9x8AluETniUWJvqMm_D8mRByqNI";
        request.setRequestURI(requestURI);
        request.setCookies(new Cookie("token", jwtExpired));
        String expectedLog1 = "【SERVICE-START】" + requestURI;
        String expectedLog2 = "【AUTHORIZE-START】" + requestURI;
        String expectedLog3 = "token : " + jwtExpired;
        ListAppender<ILoggingEvent> listAppender = new ListAppender<>();
        listAppender.start();
        logger.addAppender(listAppender);
        doReturn(jwtExpired).when(this.provider).getToken(request);
        doReturn(false).when(this.provider).validateToken(jwtExpired);

        // execute
        boolean actual = this.target.preHandle(request, response, new Object());
        listAppender.stop();

        // assert
        assertAll(() -> {
            assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getStatus());
        }, () -> {
            assertFalse(actual);
        }, () -> {
            assertEquals(3, listAppender.list.size());
        }, () -> {
            assertEquals(expectedLog1, listAppender.list.get(0).getMessage());
        }, () -> {
            assertEquals(Level.INFO, listAppender.list.get(0).getLevel());
        }, () -> {
            assertEquals(expectedLog2, listAppender.list.get(1).getMessage());
        }, () -> {
            assertEquals(Level.INFO, listAppender.list.get(1).getLevel());
        }, () -> {
            assertEquals(expectedLog3, listAppender.list.get(2).getMessage());
        }, () -> {
            assertEquals(Level.INFO, listAppender.list.get(2).getLevel());
        }, () -> {
            verify(this.provider, times(1)).getToken(request);
        }, () -> {
            verify(this.provider, times(1)).validateToken(jwtExpired);
        });
    }

    @Test
    @DisplayName("プリフライトリクエスト：HTTPステータス200を設定、trueを返却")
    void preHandlePreflightRequestTest() throws IOException {

        // setup
        Logger logger = (Logger) LoggerFactory.getLogger(JWTAuthenticationHandlerInterceptor.class);
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        String requestURI = "/api/test";
        request.setRequestURI(requestURI);
        request.setMethod(Methods.OPTIONS_STRING);
        String expectedLog1 = "【SERVICE-START】" + requestURI;
        String expectedLog2 = "【PREFLIGHT-REQUEST】" + requestURI;
        ListAppender<ILoggingEvent> listAppender = new ListAppender<>();
        listAppender.start();
        logger.addAppender(listAppender);

        // execute
        boolean actual = this.target.preHandle(request, response, new Object());
        listAppender.stop();

        // assert
        assertAll(() -> {
            assertEquals(HttpStatus.OK.value(), response.getStatus());
        }, () -> {
            assertTrue(actual);
        }, () -> {
            assertEquals(2, listAppender.list.size());
        }, () -> {
            assertEquals(expectedLog1, listAppender.list.get(0).getMessage());
        }, () -> {
            assertEquals(Level.INFO, listAppender.list.get(0).getLevel());
        }, () -> {
            assertEquals(expectedLog2, listAppender.list.get(1).getMessage());
        }, () -> {
            assertEquals(Level.INFO, listAppender.list.get(1).getLevel());
        }, () -> {
            verify(this.provider, times(0)).getToken(request);
        });
    }

    @Test
    @DisplayName("/auth/**リクエスト：HTTPステータス200を設定、trueを返却")
    void preHandleAuthRequestTest() throws IOException {

        // setup
        Logger logger = (Logger) LoggerFactory.getLogger(JWTAuthenticationHandlerInterceptor.class);
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        String requestURI = "/auth/test";
        request.setServletPath(requestURI);
        request.setRequestURI(requestURI);
        String expectedLog1 = "【SERVICE-START】" + requestURI;
        String expectedLog2 = "【AUTH-REQUEST】" + requestURI;
        ListAppender<ILoggingEvent> listAppender = new ListAppender<>();
        listAppender.start();
        logger.addAppender(listAppender);

        // execute
        boolean actual = this.target.preHandle(request, response, new Object());
        listAppender.stop();

        // assert
        assertAll(() -> {
            assertEquals(HttpStatus.OK.value(), response.getStatus());
        }, () -> {
            assertTrue(actual);
        }, () -> {
            assertEquals(2, listAppender.list.size());
        }, () -> {
            assertEquals(expectedLog1, listAppender.list.get(0).getMessage());
        }, () -> {
            assertEquals(Level.INFO, listAppender.list.get(0).getLevel());
        }, () -> {
            assertEquals(expectedLog2, listAppender.list.get(1).getMessage());
        }, () -> {
            assertEquals(Level.INFO, listAppender.list.get(1).getLevel());
        }, () -> {
            verify(this.provider, times(0)).getToken(request);
        });
    }

    @Test
    @DisplayName("リクエストURIがログ出力されること")
    void postHandleTest() {

        // setup
        Logger logger = (Logger) LoggerFactory.getLogger(JWTAuthenticationHandlerInterceptor.class);
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        String requestURI = "/api/test";
        request.setRequestURI(requestURI);
        String expectedLog = "【SERVICE-END】" + requestURI;
        ListAppender<ILoggingEvent> listAppender = new ListAppender<>();
        listAppender.start();
        logger.addAppender(listAppender);

        // execute
        this.target.postHandle(request, response, new Object(), new ModelAndView());
        listAppender.stop();

        // assert
        assertAll(() -> {
            assertEquals(1, listAppender.list.size());
        }, () -> {
            assertEquals(expectedLog, listAppender.list.get(0).getMessage());
        }, () -> {
            assertEquals(Level.INFO, listAppender.list.get(0).getLevel());
        });
    }
}
