package com.portfolio.jinkan.common;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UtilityTest {

    @Autowired
    private Utility target;

    @Test
    @DisplayName("画像データを画像URLに変換し返却すること")
    void image2UrlTest() {

        // setup
        byte[] image = "test".getBytes();
        String imageType = "image/jpeg";

        // execute
        String actual = this.target.image2Url(image, imageType);

        // assert
        assertEquals("data:image/jpeg;base64,dGVzdA==", actual);
    }

    @Test
    @DisplayName("画像データがnullの場合、nullを返却すること")
    void image2UrlNullTest() {

        // setup
        String imageType = "image/jpeg";

        // execute
        String actual = this.target.image2Url(null, imageType);

        // assert
        assertNull(actual);
    }
}
