package com.portfolio.jinkan.common;

import com.github.tomakehurst.wiremock.WireMockServer;

import org.springframework.stereotype.Component;

@Component
public class WiremockTestUtil {

    private WireMockServer server;

    /**
     * wiremock起動。
     */
    public void start() {
        this.server = new WireMockServer(14400);
        this.server.start();
    }

    /**
     * wiremock停止。
     */
    public void stop() {
        this.server.stop();
    }
}