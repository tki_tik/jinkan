package com.portfolio.jinkan.domain.service;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.portfolio.jinkan.domain.model.UserAddressInfoModel;
import com.portfolio.jinkan.domain.model.UserInfoModel;
import com.portfolio.jinkan.domain.model.UserModel;
import com.portfolio.jinkan.domain.model.UserSettingInfoModel;
import com.portfolio.jinkan.domain.repository.UserAddressInfoRepository;
import com.portfolio.jinkan.domain.repository.UserInfoRepository;
import com.portfolio.jinkan.domain.repository.UserSettingInfoRepository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class GetUserInfoServiceTest {

    @InjectMocks
    private GetUserInfoService target;

    @Mock
    private UserInfoRepository userInfoRepository;

    @Mock
    private UserAddressInfoRepository userAddressInfoRepository;

    @Mock
    private UserSettingInfoRepository userSettingInfoRepository;

    @Test
    @DisplayName("ユーザIDに紐づくユーザ権限を正常に取得できること")
    void getUserInfoTest() {

        // setup
        String userId = "999999";
        String expectedDispNm = "あどみん　次郎";
        String expectedProfileImage = null;
        String expectedState = "東京都";
        String expectedCity = "中央区";
        String expectedStation = "人形町駅";
        String expectedLocale = "ja";
        String expectedSearchTag = "java e2e";
        String expectedPortfolioUrl = "https://gitlab.com/tki_tik/auth-api-java";
        doReturn(this.createUserInfoModelStub()).when(this.userInfoRepository).getUserInfo(userId);
        doReturn(this.createUserAddressInfoModelStub()).when(this.userAddressInfoRepository).getUserAddressInfo(userId);
        doReturn(this.createUserSettingInfoModelStub()).when(this.userSettingInfoRepository).getUserSettingInfo(userId);

        // execute
        UserModel actual = this.target.getUserInfo(userId);

        // assert
        assertAll(() -> {
            assertEquals(expectedDispNm, actual.getUserInfo().getDispNm());
        }, () -> {
            assertEquals(expectedProfileImage, actual.getUserInfo().getProfileImage());
        }, () -> {
            assertEquals(expectedState, actual.getUserAddressInfo().getState());
        }, () -> {
            assertEquals(expectedCity, actual.getUserAddressInfo().getCity());
        }, () -> {
            assertEquals(expectedStation, actual.getUserAddressInfo().getStation());
        }, () -> {
            assertEquals(expectedLocale, actual.getUserSettingInfo().getLocale());
        }, () -> {
            assertEquals(expectedSearchTag, actual.getUserSettingInfo().getSearchTag());
        }, () -> {
            assertEquals(expectedPortfolioUrl, actual.getUserSettingInfo().getPortfolioUrl());
        }, () -> {
            verify(this.userInfoRepository, times(1)).getUserInfo(userId);
        }, () -> {
            verify(this.userAddressInfoRepository, times(1)).getUserAddressInfo(userId);
        }, () -> {
            verify(this.userSettingInfoRepository, times(1)).getUserSettingInfo(userId);
        });
    }

    private UserInfoModel createUserInfoModelStub() {
        return UserInfoModel.builder()
            .dispNm("あどみん　次郎")
            .profileImage(null)
            .build();
    }

    private UserAddressInfoModel createUserAddressInfoModelStub() {
        return UserAddressInfoModel.builder()
            .state("東京都")
            .city("中央区")
            .station("人形町駅")
            .build();
    }

    private UserSettingInfoModel createUserSettingInfoModelStub() {
        return UserSettingInfoModel.builder()
            .locale("ja")
            .searchTag("java e2e")
            .portfolioUrl("https://gitlab.com/tki_tik/auth-api-java")
            .build();
    }
}
