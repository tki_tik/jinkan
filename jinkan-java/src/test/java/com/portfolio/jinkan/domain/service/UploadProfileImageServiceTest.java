package com.portfolio.jinkan.domain.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.FileInputStream;
import java.io.IOException;

import com.portfolio.jinkan.domain.repository.UserInfoRepository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;

@SpringBootTest
public class UploadProfileImageServiceTest {

    @InjectMocks
    private UploadProfileImageService target;

    @Mock
    private UserInfoRepository repository;

    @Test
    @DisplayName("プロフィール画像がnullの場合、ユーザ情報更新処理を呼び出すこと")
    void uploadProfileImageNullTest() throws IOException {

        // setup
        String userId = "999999";
        doReturn(1).when(this.repository).updateProfileImage(userId);

        // execute
        this.target.uploadProfileImage(userId, null);

        // assert
        verify(this.repository, times(1)).updateProfileImage(userId);
    }

    @Test
    @DisplayName("プロフィール画像がnullでない場合、ユーザ情報更新処理を呼び出すこと")
    void uploadProfileImageTest() throws IOException {

        // setup
        String userId = "000001";
        FileInputStream inputFile = new FileInputStream("src/test/resources/file/test.jpg");
        MockMultipartFile profileImage =
            new MockMultipartFile("test.jpg", "test.jpg", MediaType.IMAGE_JPEG_VALUE, inputFile);
        doReturn(1).when(this.repository).updateProfileImage(userId, profileImage);

        // execute
        this.target.uploadProfileImage(userId, profileImage);

        // assert
        verify(this.repository, times(1)).updateProfileImage(userId, profileImage);
    }

    @Test
    @DisplayName("ユーザ情報更新処理の更新結果が0件の場合、RuntimeExceptionをスローすること")
    void uploadProfileImageFailureTest() throws IOException {

        // setup
        String userId = "000000";
        doReturn(0).when(this.repository).updateProfileImage(userId);

        // execute & assert
        assertThrows(RuntimeException.class, () -> {
            this.target.uploadProfileImage(userId, null);
        });

        // assert
        verify(this.repository, times(1)).updateProfileImage(userId);
    }
}
