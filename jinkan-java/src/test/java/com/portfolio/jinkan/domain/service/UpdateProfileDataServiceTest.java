package com.portfolio.jinkan.domain.service;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.portfolio.jinkan.domain.model.UpdateProfileDataModel;
import com.portfolio.jinkan.domain.repository.UserAddressInfoRepository;
import com.portfolio.jinkan.domain.repository.UserInfoRepository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UpdateProfileDataServiceTest {

    @InjectMocks
    private UpdateProfileDataService target;

    @Mock
    private UserInfoRepository userInfoRepository;

    @Mock
    private UserAddressInfoRepository userAddressInfoRepository;

    @Test
    @DisplayName("ユーザ情報更新処理及びユーザ住所情報更新処理を呼び出すこと")
    void updateProfileTest() {

        // setup
        String userId = "999999";
        UpdateProfileDataModel model = UpdateProfileDataModel.builder()
            .userId(userId)
            .dispNm("アドミン")
            .state("埼玉県")
            .city("さいたま市")
            .station("大宮駅")
            .build();
        doReturn(1).when(this.userInfoRepository).updateDispNm(model.getUserId(), model.getDispNm());
        doReturn(1).when(this.userAddressInfoRepository).update(model);

        // execute
        this.target.updateProfile(model);

        // assert
        assertAll(() -> {
            verify(this.userInfoRepository, times(1)).updateDispNm(model.getUserId(), model.getDispNm());
        }, () -> {
            verify(this.userAddressInfoRepository, times(1)).update(model);
        });
    }

    @Test
    @DisplayName("ユーザ情報更新処理の更新結果が0件の場合、RuntimeExceptionをスローすること")
    void updateProfileUserInfoUpdateFailureTest() {

        // setup
        String userId = "999999";
        UpdateProfileDataModel model = UpdateProfileDataModel.builder()
            .userId(userId)
            .dispNm("アドミン")
            .state("埼玉県")
            .city("さいたま市")
            .station("大宮駅")
            .build();
        doReturn(0).when(this.userInfoRepository).updateDispNm(model.getUserId(), model.getDispNm());

        // execute & assert
        assertThrows(RuntimeException.class, () -> {
            this.target.updateProfile(model);
        });

        // assert
        verify(this.userAddressInfoRepository, times(0)).update(model);
    }

    @Test
    @DisplayName("ユーザ住所情報更新処理の更新結果が0件の場合、RuntimeExceptionをスローすること")
    void updateProfileUserAddressInfoUpdateFailureTest() {

        // setup
        String userId = "999999";
        UpdateProfileDataModel model = UpdateProfileDataModel.builder()
            .userId(userId)
            .dispNm("アドミン")
            .state("埼玉県")
            .city("さいたま市")
            .station("大宮駅")
            .build();
        doReturn(1).when(this.userInfoRepository).updateDispNm(model.getUserId(), model.getDispNm());
        doReturn(0).when(this.userAddressInfoRepository).update(model);

        // execute & assert
        assertThrows(RuntimeException.class, () -> {
            this.target.updateProfile(model);
        });

        // assert
        assertAll(() -> {
            verify(this.userInfoRepository, times(1)).updateDispNm(model.getUserId(), model.getDispNm());
        }, () -> {
            verify(this.userAddressInfoRepository, times(1)).update(model);
        });
    }
}
