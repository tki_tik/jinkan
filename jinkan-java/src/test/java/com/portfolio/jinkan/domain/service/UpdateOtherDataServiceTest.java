package com.portfolio.jinkan.domain.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.portfolio.jinkan.domain.model.UpdateOtherDataModel;
import com.portfolio.jinkan.domain.repository.UserSettingInfoRepository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UpdateOtherDataServiceTest {

    @InjectMocks
    private UpdateOtherDataService target;

    @Mock
    private UserSettingInfoRepository repository;

    @Test
    @DisplayName("ユーザ設定情報更新処理を呼び出すこと")
    void updateOtherTest() {

        // setup
        String userId = "999999";
        UpdateOtherDataModel model = UpdateOtherDataModel.builder()
            .userId(userId)
            .locale("zh")
            .searchTag("python")
            .portfolioUrl("https://gitlab.com/test")
            .build();
        doReturn(1).when(this.repository).update(model);

        // execute
        this.target.updateOther(model);

        // assert
        verify(this.repository, times(1)).update(model);
    }

    @Test
    @DisplayName("ユーザ設定情報更新処理の更新結果が0件の場合、RuntimeExceptionをスローすること")
    void updateOtherFailureTest() {

        // setup
        String userId = "999999";
        UpdateOtherDataModel model = UpdateOtherDataModel.builder()
            .userId(userId)
            .locale("zh")
            .searchTag("python")
            .portfolioUrl("https://gitlab.com/test")
            .build();
        doReturn(0).when(this.repository).update(model);

        // execute & assert
        assertThrows(RuntimeException.class, () -> {
            this.target.updateOther(model);
        });

        // assert
        verify(this.repository, times(1)).update(model);
    }
}
