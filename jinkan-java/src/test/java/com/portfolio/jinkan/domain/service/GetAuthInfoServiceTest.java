package com.portfolio.jinkan.domain.service;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.UUID;

import com.portfolio.jinkan.domain.model.AuthInfoModel;
import com.portfolio.jinkan.domain.repository.client.GetAuthInfoRepository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class GetAuthInfoServiceTest {

    @InjectMocks
    private GetAuthInfoService target;

    @Mock
    private GetAuthInfoRepository repository;

    @Test
    @DisplayName("認可コードに紐づく認証情報を正常に取得できること")
    void getAuthInfoTest() {

        // setup
        String code = UUID.randomUUID().toString();
        String userId = "999999";
        String userName = "admin";
        String role = "ROLE_ADMIN";
        AuthInfoModel authInfo = AuthInfoModel.builder().userId(userId).userName(userName).role(role).build();
        doReturn(authInfo).when(this.repository).getAuthInfo(code);

        // execute
        AuthInfoModel actual = this.target.getAuthInfo(code);

        // assert
        assertAll(() -> {
            assertEquals(userId, actual.getUserId());
        }, () -> {
            assertEquals(userName, actual.getUserName());
        }, () -> {
            assertEquals(role, actual.getRole());
        }, () -> {
            verify(this.repository, times(1)).getAuthInfo(code);
        });
    }
}
