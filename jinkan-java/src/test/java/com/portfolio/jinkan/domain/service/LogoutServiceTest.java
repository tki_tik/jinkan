package com.portfolio.jinkan.domain.service;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.portfolio.jinkan.domain.repository.client.LogoutRepository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class LogoutServiceTest {

    @InjectMocks
    private LogoutService target;

    @Mock
    private LogoutRepository repository;

    @Test
    @DisplayName("ログアウト処理を呼び出すこと")
    void getAuthInfoTest() {

        // setup
        doNothing().when(this.repository).logout();

        // execute
        this.target.doLogout();

        // assert
        verify(this.repository, times(1)).logout();
    }
}
