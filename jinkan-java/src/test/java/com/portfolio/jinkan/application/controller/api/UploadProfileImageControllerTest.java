package com.portfolio.jinkan.application.controller.api;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;

import com.portfolio.jinkan.common.Utility;
import com.portfolio.jinkan.common.configuration.CorsConfiguration;
import com.portfolio.jinkan.common.interceptor.JWTAuthenticationHandlerInterceptor;
import com.portfolio.jinkan.domain.service.UploadProfileImageService;

import org.apache.commons.lang.StringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UploadProfileImageController.class)
@Import({
    CorsConfiguration.class
})
public class UploadProfileImageControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private JWTAuthenticationHandlerInterceptor interceptor;

    @MockBean
    private UploadProfileImageService service;

    @MockBean
    private Utility utility;

    @Test
    @DisplayName("/api/upload-profile-imageリクエストでユーザ情報更新処理を呼び出し、更新プロフィール画像URLを返却すること")
    void uploadImageTest() throws Exception {

        // setup
        String userId = "999999";
        FileInputStream inputFile = new FileInputStream("src/test/resources/file/test.jpg");
        MockMultipartFile profileImage =
            new MockMultipartFile("test.jpg", "test.jpg", MediaType.IMAGE_JPEG_VALUE, inputFile);
        doReturn(true).when(this.interceptor).preHandle(any(), any(), any());
        doNothing().when(this.service).uploadProfileImage(eq(userId), any());
        doReturn("test").when(this.utility).image2Url(any(), any());

        // execute & assert
        MvcResult result = this.mockMvc.perform(multipart("/api/upload-profile-image")
            .file("profileImage", profileImage.getBytes())
            .params(this.createRequest(userId)))
            .andExpect(status().isOk())
            .andReturn();

        // assert
        assertAll(() -> {
            assertEquals("test", result.getResponse().getContentAsString(StandardCharsets.UTF_8));
        }, () -> {
            verify(this.service, times(1)).uploadProfileImage(eq(userId), any());
        }, () -> {
            verify(this.utility, times(1)).image2Url(any(), any());
        });
    }

    @Test
    @DisplayName("/api/upload-profile-imageリクエストでユーザ情報更新処理を呼び出し、nullを返却すること")
    void uploadImageNullTest() throws Exception {

        // setup
        String userId = "999999";
        doReturn(true).when(this.interceptor).preHandle(any(), any(), any());
        doNothing().when(this.service).uploadProfileImage(userId, null);

        // execute & assert
        MvcResult result = this.mockMvc.perform(multipart("/api/upload-profile-image")
            .params(this.createRequest(userId)))
            .andExpect(status().isOk())
            .andReturn();

        // assert
        assertAll(() -> {
            assertEquals(StringUtils.EMPTY, result.getResponse().getContentAsString());
        }, () -> {
            verify(this.service, times(1)).uploadProfileImage(userId, null);
        }, () -> {
            verify(this.utility, times(0)).image2Url(any(), any());
        });
    }

    private MultiValueMap<String, String> createRequest(String userId) {
        MultiValueMap<String, String> valueMap = new LinkedMultiValueMap<>();
        valueMap.add("userId", userId);
        return valueMap;
    }
}
