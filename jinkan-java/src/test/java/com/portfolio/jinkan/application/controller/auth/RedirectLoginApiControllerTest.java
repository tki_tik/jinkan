package com.portfolio.jinkan.application.controller.auth;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.portfolio.jinkan.common.configuration.AuthapiConfiguration;
import com.portfolio.jinkan.common.configuration.CorsConfiguration;
import com.portfolio.jinkan.common.interceptor.JWTAuthenticationHandlerInterceptor;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@ExtendWith(SpringExtension.class)
@WebMvcTest(RedirectLoginApiController.class)
@Import({
    CorsConfiguration.class
})
public class RedirectLoginApiControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private JWTAuthenticationHandlerInterceptor interceptor;

    @MockBean
    private AuthapiConfiguration configuration;

    @Test
    @DisplayName("/auth/redirect-login-apiリクエストでログイン用APIURLへリダイレクトすること")
    void refreshTokenTest() throws Exception {

        // setup
        String loginUrl = "http://localhost:14400/login";
        String clientId = "loginapi";
        String expectedLoginUrl = loginUrl + "?clientId=" + clientId;
        doReturn(true).when(this.interceptor).preHandle(any(), any(), any());
        doReturn(loginUrl).when(this.configuration).getLoginUrl();
        doReturn(clientId).when(this.configuration).getClientId();

        // execute
        MvcResult result = this.mockMvc.perform(get("/auth/redirect-login-api"))
            .andExpect(status().isFound())
            .andReturn();

        // assert
        assertAll(() -> {
            assertEquals(expectedLoginUrl, result.getResponse().getRedirectedUrl());
        }, () -> {
            verify(this.configuration, times(1)).getLoginUrl();
        }, () -> {
            verify(this.configuration, times(1)).getClientId();
        });
        
    }
}
