package com.portfolio.jinkan.application.controller.api;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.portfolio.jinkan.common.configuration.CorsConfiguration;
import com.portfolio.jinkan.common.interceptor.JWTAuthenticationHandlerInterceptor;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@WebMvcTest(RefreshTokenController.class)
@Import({
    CorsConfiguration.class
})
public class RefreshTokenControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private JWTAuthenticationHandlerInterceptor interceptor;

    @Test
    @DisplayName("/api/refresh-tokenリクエストでinterceptorを呼び出すこと")
    void refreshTokenTest() throws Exception {

        // setup
        doReturn(true).when(this.interceptor).preHandle(any(), any(), any());

        // execute
        this.mockMvc.perform(post("/api/refresh-token"))
            .andExpect(status().isOk());

        // assert
        verify(this.interceptor, times(1)).preHandle(any(), any(), any());
    }
}
