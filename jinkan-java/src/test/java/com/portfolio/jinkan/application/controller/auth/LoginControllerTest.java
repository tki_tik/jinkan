package com.portfolio.jinkan.application.controller.auth;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;

import com.portfolio.jinkan.common.JWTProvider;
import com.portfolio.jinkan.common.configuration.CorsConfiguration;
import com.portfolio.jinkan.common.interceptor.JWTAuthenticationHandlerInterceptor;
import com.portfolio.jinkan.domain.model.AuthInfoModel;
import com.portfolio.jinkan.domain.service.GetAuthInfoService;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@WebMvcTest(LoginController.class)
@Import({
    CorsConfiguration.class
})
public class LoginControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private JWTAuthenticationHandlerInterceptor interceptor;

    @MockBean
    private GetAuthInfoService service;

    @MockBean
    private JWTProvider provider;

    @Test
    @DisplayName("/auth/loginリクエストでLogoutServiceを呼び出すこと")
    void refreshTokenTest() throws Exception {

        // setup
        String code = UUID.randomUUID().toString();
        AuthInfoModel authInfo = this.createAuthInfoModelStub();
        String jwt = "jwt-token";
        doReturn(true).when(this.interceptor).preHandle(any(), any(), any());
        doReturn(authInfo).when(this.service).getAuthInfo(code);
        doReturn(jwt).when(this.provider).createToken(authInfo);
        doNothing().when(this.provider).setToken(any(), eq(jwt));

        // execute
        this.mockMvc.perform(get("/auth/login").param("code", code))
            .andExpect(status().isOk())
            .andReturn();

        // assert
        assertAll(() -> {
            verify(this.service, times(1)).getAuthInfo(code);
        }, () -> {
            verify(this.provider, times(1)).createToken(authInfo);
        }, () -> {
            verify(this.provider, times(1)).setToken(any(), eq(jwt));
        });
    }

    private AuthInfoModel createAuthInfoModelStub() {
        return AuthInfoModel.builder()
            .userId("000000")
            .userName("user")
            .role("ROLE_USER")
            .build();
    }
}
