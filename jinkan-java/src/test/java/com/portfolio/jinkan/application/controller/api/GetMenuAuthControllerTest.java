package com.portfolio.jinkan.application.controller.api;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.portfolio.jinkan.application.payload.MenuAuthResponse;
import com.portfolio.jinkan.common.JWTProvider;
import com.portfolio.jinkan.common.Utility;
import com.portfolio.jinkan.common.configuration.AuthorizeConfiguration;
import com.portfolio.jinkan.common.configuration.CorsConfiguration;
import com.portfolio.jinkan.domain.service.GetMenuInfoService;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@ExtendWith(SpringExtension.class)
@WebMvcTest(GetMenuAuthController.class)
@Import({
    JWTProvider.class,
    Utility.class,
    AuthorizeConfiguration.class,
    CorsConfiguration.class
})
public class GetMenuAuthControllerTest {

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GetMenuInfoService service;

    @Test
    @DisplayName("/api/get-menu-authリクエストでメニュー権限情報が取得できること")
    void getMenuAuthListTest() throws Exception {

        // setup
        String menuTitleTag = "home";
        String auth1 = "ROLE_ADMIN";
        String auth2 = "ROLE_USER";
        List<String> menuAuthList = Arrays.asList(auth1, auth2);
        String expectedContent = this.mapper.writeValueAsString(
            MenuAuthResponse.builder().authList(menuAuthList).build()
        );
        doReturn(menuAuthList).when(this.service).getMenuAuthList(menuTitleTag);

        // execute
        MvcResult result = this.mockMvc.perform(get("/api/get-menu-auth").param("menuTitleTag", menuTitleTag))
            .andExpect(status().isOk())
            .andReturn();

        // assert
        assertAll(() -> {
            assertEquals(expectedContent, result.getResponse().getContentAsString(StandardCharsets.UTF_8));
        }, () -> {
            verify(this.service, times(1)).getMenuAuthList(menuTitleTag);
        });
    }
}
