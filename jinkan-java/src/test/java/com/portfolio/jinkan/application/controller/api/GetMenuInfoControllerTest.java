package com.portfolio.jinkan.application.controller.api;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.portfolio.jinkan.application.payload.MenuInfo;
import com.portfolio.jinkan.application.payload.MenuInfoResponse;
import com.portfolio.jinkan.common.configuration.CorsConfiguration;
import com.portfolio.jinkan.common.interceptor.JWTAuthenticationHandlerInterceptor;
import com.portfolio.jinkan.domain.model.MenuInfoModel;
import com.portfolio.jinkan.domain.service.GetMenuInfoService;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@ExtendWith(SpringExtension.class)
@WebMvcTest(GetMenuInfoController.class)
@Import({
    CorsConfiguration.class
})
public class GetMenuInfoControllerTest {

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GetMenuInfoService service;

    @MockBean
    private JWTAuthenticationHandlerInterceptor interceptor;

    @Test
    @DisplayName("/api/get-menu-infoリクエストでメニュー情報が取得できること")
    void getMenuInfoListTest() throws Exception {

        // setup
        String auth = "ROLE_USER";
        String expectedContent = this.mapper.writeValueAsString(
            MenuInfoResponse.builder()
                .menuInfoList(this.createMenuInfoListStub())
                .build()
        );
        doReturn(true).when(this.interceptor).preHandle(any(), any(), any());
        doReturn(this.createMenuInfoModelListStub()).when(this.service).getMenuInfoList(auth);

        // execute
        MvcResult result = this.mockMvc.perform(get("/api/get-menu-info")
            .param("auth", auth))
            .andExpect(status().isOk())
            .andReturn();

        // assert
        assertAll(() -> {
            assertEquals(expectedContent, result.getResponse().getContentAsString(StandardCharsets.UTF_8));
        }, () -> {
            verify(this.interceptor, times(1)).preHandle(any(), any(), any());
        }, () -> {
            verify(this.service, times(1)).getMenuInfoList(auth);
        });
    }

    private List<MenuInfo> createMenuInfoListStub() {
        List<MenuInfo> stubList = new ArrayList<>();
        stubList.add(
            MenuInfo.builder()
                .id("001")
                .url("/")
                .path("M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6")
                .titleTag("home")
                .build()
        );
        stubList.add(
            MenuInfo.builder()
                .id("002")
                .url("/employee-search")
                .path("M8 16l2.879-2.879m0 0a3 3 0 104.243-4.242 3 3 0 00-4.243 4.242zM21 12a9 9 0 11-18 0 9 9 0 0118 0z")
                .titleTag("employee")
                .build()
        );
        return stubList;
    }

    private List<MenuInfoModel> createMenuInfoModelListStub() {
        List<MenuInfoModel> stubList = new ArrayList<>();
        stubList.add(
            MenuInfoModel.builder()
                .id("001")
                .url("/")
                .path("M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6")
                .titleTag("home")
                .build()
        );
        stubList.add(
            MenuInfoModel.builder()
                .id("002")
                .url("/employee-search")
                .path("M8 16l2.879-2.879m0 0a3 3 0 104.243-4.242 3 3 0 00-4.243 4.242zM21 12a9 9 0 11-18 0 9 9 0 0118 0z")
                .titleTag("employee")
                .build()
        );
        return stubList;
    }
}
