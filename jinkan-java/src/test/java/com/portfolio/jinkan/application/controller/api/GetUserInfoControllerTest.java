package com.portfolio.jinkan.application.controller.api;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.StandardCharsets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.portfolio.jinkan.application.payload.UserInfoResponse;
import com.portfolio.jinkan.common.Utility;
import com.portfolio.jinkan.common.configuration.CorsConfiguration;
import com.portfolio.jinkan.common.interceptor.JWTAuthenticationHandlerInterceptor;
import com.portfolio.jinkan.domain.model.UserAddressInfoModel;
import com.portfolio.jinkan.domain.model.UserInfoModel;
import com.portfolio.jinkan.domain.model.UserModel;
import com.portfolio.jinkan.domain.model.UserSettingInfoModel;
import com.portfolio.jinkan.domain.service.GetUserInfoService;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@ExtendWith(SpringExtension.class)
@WebMvcTest(GetUserInfoController.class)
@Import({
    CorsConfiguration.class
})
public class GetUserInfoControllerTest {

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private JWTAuthenticationHandlerInterceptor interceptor;

    @MockBean
    private Utility utility;

    @MockBean
    private GetUserInfoService service;

    @Test
    @DisplayName("/api/get-user-infoリクエストでユーザ情報が取得できること")
    void getUserInfoListTest() throws Exception {

        // setup
        String userId = "999999";
        String expectedContent = this.mapper.writeValueAsString(
            this.createUserInfoResponseStub()
        );
        doReturn(true).when(this.interceptor).preHandle(any(), any(), any());
        doReturn(this.createUserModelStub()).when(this.service).getUserInfo(userId);
        doReturn("testurl").when(this.utility).image2Url(any(), any());

        // execute
        MvcResult result = this.mockMvc.perform(get("/api/get-user-info")
            .param("userId", userId))
            .andExpect(status().isOk())
            .andReturn();

        // assert
        assertAll(() -> {
            assertEquals(expectedContent, result.getResponse().getContentAsString(StandardCharsets.UTF_8));
        }, () -> {
            verify(this.service, times(1)).getUserInfo(userId);
        });
    }

    private UserInfoResponse createUserInfoResponseStub() {
        return UserInfoResponse.builder()
            .dispNm("ゆーざ　次郎")
            .profileImageUrl("testurl")
            .state("東京都")
            .city("中央区")
            .station("人形町駅")
            .locale("ja")
            .searchTag("java e2e")
            .portfolioUrl("https://gitlab.com/tki_tik/auth-api-java")
            .build();
    }

    private UserModel createUserModelStub() {
        UserInfoModel userInfoModel = UserInfoModel.builder()
            .dispNm("ゆーざ　次郎")
            .profileImage(null)
            .build();

        UserAddressInfoModel userAddressInfoModel = UserAddressInfoModel.builder()
            .state("東京都")
            .city("中央区")
            .station("人形町駅")
            .build();

        UserSettingInfoModel userSettingInfoModel = UserSettingInfoModel.builder()
            .locale("ja")
            .searchTag("java e2e")
            .portfolioUrl("https://gitlab.com/tki_tik/auth-api-java")
            .build();

        return UserModel.builder()
            .userInfo(userInfoModel)
            .userAddressInfo(userAddressInfoModel)
            .userSettingInfo(userSettingInfoModel)
            .build();
    }
}
