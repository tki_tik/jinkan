package com.portfolio.jinkan.application.controller.api;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.portfolio.jinkan.application.payload.UpdateProfileDataRequest;
import com.portfolio.jinkan.common.configuration.CorsConfiguration;
import com.portfolio.jinkan.common.interceptor.JWTAuthenticationHandlerInterceptor;
import com.portfolio.jinkan.domain.service.UpdateProfileDataService;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UpdateProfileDataController.class)
@Import({
    CorsConfiguration.class
})
public class UpdateProfileDataControllerTest {

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private JWTAuthenticationHandlerInterceptor interceptor;

    @MockBean
    private UpdateProfileDataService service;

    @Test
    @DisplayName("/api/update-profile-dataリクエストでプロフィール情報更新処理を呼び出すこと")
    void updateProfileDataTest() throws JsonProcessingException, Exception {

        // setup
        UpdateProfileDataRequest request = this.createUpdateProfileDataRequestStub();
        doReturn(true).when(this.interceptor).preHandle(any(), any(), any());
        doNothing().when(this.service).updateProfile(any());

        // execute
        this.mockMvc.perform(post("/api/update-profile-data")
            .contentType(MediaType.APPLICATION_JSON)
            .content(this.mapper.writeValueAsString(request)))
            .andExpect(status().isOk());

        // assert
        verify(this.service, times(1)).updateProfile(any());
    }

    private UpdateProfileDataRequest createUpdateProfileDataRequestStub() {
        UpdateProfileDataRequest request = new UpdateProfileDataRequest();
        request.setUserId("999999");
        request.setDispNm("あどみん　太郎");
        request.setState("東京都");
        request.setCity("中央区");
        request.setStation("人形町駅");
        return request;
    }
}
