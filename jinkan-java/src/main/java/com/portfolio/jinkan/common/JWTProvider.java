package com.portfolio.jinkan.common;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.portfolio.jinkan.common.configuration.AuthorizeConfiguration;
import com.portfolio.jinkan.domain.model.AuthInfoModel;

import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.undertow.util.Headers;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class JWTProvider {

    private final Utility utility;

    private final AuthorizeConfiguration configuration;

    /**
     * JWTトークン作成
     * 
     * @param model 認証情報
     * @return JWTトークン
     */
    public String createToken(AuthInfoModel model) {
        return Jwts.builder()
            .setClaims(this.createClaims(model))
            .setIssuedAt(this.utility.getSysDate())
            .setExpiration(this.createExpireTime())
            .signWith(SignatureAlgorithm.HS256, this.configuration.getTokenSecretKey())
            .compact();
    }

    /**
     * JWTトークン有効期限検証。
     * 
     * @param token JWTトークン
     * @return 有効期限検証結果
     */
    public boolean validateToken(String token) {
        try {
            Jws<Claims> claims =
                Jwts.parser().setSigningKey(this.configuration.getTokenSecretKey()).parseClaimsJws(token);
            return !claims.getBody().getExpiration().before(this.utility.getSysDate());
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * JWTトークン更新。
     * 
     * @param token JWTトークン
     * @return JWTトークン
     */
    public String refreshToken(String token) {
        return Jwts.builder()
            .setClaims(this.getClaims(token))
            .setIssuedAt(this.utility.getSysDate())
            .setExpiration(this.createExpireTime())
            .signWith(SignatureAlgorithm.HS256, this.configuration.getTokenSecretKey())
            .compact();
    }

    /**
     * JWTトークン取得。
     * 
     * @param request HttpServletRequest
     * @return JWTトークン
     */
    public String getToken(HttpServletRequest request) {
        return request.getHeader(Headers.AUTHORIZATION_STRING);
    }

    /**
     * JWTトークン設定。
     * 
     * @param response HttpServletResponse
     * @param token    JWTトークン
     */
    public void setToken(HttpServletResponse response, String token) {
        response.setHeader(Headers.AUTHORIZATION_STRING, token);
    }

    private Claims createClaims(AuthInfoModel model) {
        Claims claims = Jwts.claims().setSubject(model.getUserId());
        claims.put("username", model.getUserName());
        claims.put("role", model.getRole());
        return claims;
    }

    private Claims getClaims(String token) {
        return Jwts.parser().setSigningKey(this.configuration.getTokenSecretKey()).parseClaimsJws(token).getBody();
    }

    private Date createExpireTime() {
        Date expireTime = this.utility.getSysDate();
        expireTime.setTime(expireTime.getTime() + this.configuration.getTokenValidDuration());
        return expireTime;
    }
}
