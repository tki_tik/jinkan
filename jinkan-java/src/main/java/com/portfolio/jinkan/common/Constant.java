package com.portfolio.jinkan.common;

public class Constant {

    /**
     * コンストラクタ。
     * インスタンス化させない為private。
     */
    private Constant() {
    }

    /** リダイレクト用接頭辞 */
    public static String REDIRECT_PREFIX = "redirect:";

    /** 認証処置用接頭辞 */
    public static String AUTH_PREFIX = "/auth";

    /** クッキー名：トークン */
    public static String COOKIE_NAME_TOKEN = "token";

    /** クエリーキー：認可コード */
    public static String QUERY_KEY_CODE = "code";

    /** クエリーキー：認可コード */
    public static String QUERY_KEY_CLIENT_ID = "clientId";
}
