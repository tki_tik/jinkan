package com.portfolio.jinkan.common.configuration;

import com.portfolio.jinkan.common.interceptor.JWTAuthenticationHandlerInterceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {

    private final JWTAuthenticationHandlerInterceptor interceptor;

    private final CorsConfiguration configuration;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(interceptor).addPathPatterns("/**").excludePathPatterns("/error", "/api/get-menu-auth");
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
            .allowedOrigins(this.configuration.getAllowedOriginList())
            .allowedHeaders("*")
            .allowedMethods("*")
            .exposedHeaders("*")
            .allowCredentials(true);
    }
}
