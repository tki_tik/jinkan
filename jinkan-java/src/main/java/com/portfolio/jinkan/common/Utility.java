package com.portfolio.jinkan.common;

import java.util.Base64;
import java.util.Date;

import org.springframework.stereotype.Component;

import lombok.Generated;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component
public class Utility {

    /**
     * システム日付取得。
     * 
     * @return 本日日付
     */
    @Generated
    public Date getSysDate() {
        return new Date();
    }

    public String image2Url(byte[] image, String imageType) {
        if (image == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("data:");
        sb.append(imageType);
        sb.append(";base64,");
        sb.append(Base64.getEncoder().encodeToString(image));
        return sb.toString();
    }
}
