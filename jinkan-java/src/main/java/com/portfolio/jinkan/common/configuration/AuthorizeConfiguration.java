package com.portfolio.jinkan.common.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@ConfigurationProperties(prefix = "authorize")
@Data
@NoArgsConstructor
public class AuthorizeConfiguration {

    /** トークン用秘密キー */
    private String tokenSecretKey;

    /** トークン有効期間 */
    private long tokenValidDuration;
}
