package com.portfolio.jinkan.common.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@ConfigurationProperties(prefix = "authapi")
@Data
@NoArgsConstructor
public class AuthapiConfiguration {

    /** クライアントID */
    private String clientId;

    /** ログイン用URL */
    private String loginUrl;

    /** 認証情報取得API用URL */
    private String getAuthInfoUrl;

    /** ログアウト用URL */
    private String logoutUrl;
}
