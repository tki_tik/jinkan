package com.portfolio.jinkan.common.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@ConfigurationProperties(prefix = "spring.cors")
@Data
@NoArgsConstructor
public class CorsConfiguration {

    /** 許可オリジンリスト */
    String[] allowedOriginList;
}
