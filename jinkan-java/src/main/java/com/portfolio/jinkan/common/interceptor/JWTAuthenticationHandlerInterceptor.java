package com.portfolio.jinkan.common.interceptor;

import static com.portfolio.jinkan.common.Constant.AUTH_PREFIX;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.portfolio.jinkan.common.JWTProvider;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import io.undertow.util.Methods;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Component
public class JWTAuthenticationHandlerInterceptor implements HandlerInterceptor {

    private final JWTProvider provider;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
        throws IOException {
        log.info("【SERVICE-START】" + request.getRequestURI());

        // PREFLIGHT-REQUESTの場合、認証を行わない
        if (Methods.OPTIONS_STRING.equals(request.getMethod())) {
            log.info("【PREFLIGHT-REQUEST】" + request.getRequestURI());
            response.setStatus(HttpStatus.OK.value());
            return true;
        }

        // /auth/**リクエストの場合、認証を行わない
        if (request.getServletPath().startsWith(AUTH_PREFIX)) {
            log.info("【AUTH-REQUEST】" + request.getRequestURI());
            response.setStatus(HttpStatus.OK.value());
            return true;
        }

        // 認証
        log.info("【AUTHORIZE-START】" + request.getRequestURI());
        String token = this.provider.getToken(request);
        log.info("token : " + token);
        if (this.provider.validateToken(token)) {
            String newToken = this.provider.refreshToken(token);
            this.provider.setToken(response, newToken);
            log.info("【AUTHORIZE-END】" + request.getRequestURI());
            response.setStatus(HttpStatus.OK.value());
            return true;
        }
        response.sendError(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED.getReasonPhrase());
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
        ModelAndView modelAndView) {
        log.info("【SERVICE-END】" + request.getRequestURI());
    }
}
