package com.portfolio.jinkan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JinkanApplication {

	public static void main(String[] args) {
		SpringApplication.run(JinkanApplication.class, args);
	}

}
