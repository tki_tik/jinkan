package com.portfolio.jinkan.application.controller.api;

import java.io.IOException;

import com.portfolio.jinkan.application.payload.UploadProfileImageRequest;
import com.portfolio.jinkan.common.Utility;
import com.portfolio.jinkan.domain.service.UploadProfileImageService;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RequestMapping("/api")
@RestController
public class UploadProfileImageController {

    private final Utility utility;

    private final UploadProfileImageService service;

    /**
     * プロフィール画像更新。
     * 
     * @param request リクエスト情報
     * @return 更新プロフィール画像URL
     * @throws IOException バイナリ変換に失敗した場合
     */
    @PostMapping(value = "/upload-profile-image", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public String uploadImage(UploadProfileImageRequest request) throws IOException {
        this.service.uploadProfileImage(request.getUserId(), request.getProfileImage());

        if (request.getProfileImage() == null) {
            return null;
        }

        return this.utility.image2Url(
            request.getProfileImage().getBytes(),
            request.getProfileImage().getContentType()
        );
    }
}
