package com.portfolio.jinkan.application.payload;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class UploadProfileImageRequest {

    /** ユーザID */
    private String userId;

    /** プロフィール画像 */
    private MultipartFile profileImage;
}
