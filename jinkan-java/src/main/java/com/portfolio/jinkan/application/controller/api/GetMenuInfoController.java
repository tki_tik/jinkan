package com.portfolio.jinkan.application.controller.api;

import java.util.ArrayList;
import java.util.List;

import com.portfolio.jinkan.application.payload.MenuInfo;
import com.portfolio.jinkan.application.payload.MenuInfoResponse;
import com.portfolio.jinkan.domain.model.MenuInfoModel;
import com.portfolio.jinkan.domain.service.GetMenuInfoService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RequestMapping("/api")
@RequiredArgsConstructor
@RestController
public class GetMenuInfoController {

    private final GetMenuInfoService service;

    /**
     * メニュー情報取得。
     * 
     * @param auth 権限
     * @return メニュー情報
     */
    @GetMapping("/get-menu-info")
    public MenuInfoResponse getMenuInfoList(@RequestParam(name = "auth") String auth) {
        List<MenuInfoModel> menuInfoModelList = this.service.getMenuInfoList(auth);
        List<MenuInfo> menuInfoList = new ArrayList<>();
        for (MenuInfoModel model : menuInfoModelList) {
            menuInfoList.add(MenuInfo.builder()
                .id(model.getId())
                .url(model.getUrl())
                .path(model.getPath())
                .titleTag(model.getTitleTag())
                .build()
            );
        }
        return MenuInfoResponse.builder().menuInfoList(menuInfoList).build();
    }
}
