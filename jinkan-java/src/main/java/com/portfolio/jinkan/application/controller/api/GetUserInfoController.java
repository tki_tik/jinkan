package com.portfolio.jinkan.application.controller.api;

import com.portfolio.jinkan.application.payload.UserInfoResponse;
import com.portfolio.jinkan.common.Utility;
import com.portfolio.jinkan.domain.model.UserModel;
import com.portfolio.jinkan.domain.service.GetUserInfoService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RequestMapping("/api")
@RequiredArgsConstructor
@RestController
public class GetUserInfoController {

    private final Utility utility;

    private final GetUserInfoService service;

    /**
     * ユーザ情報取得。
     * 
     * @param userId ユーザID
     * @return ユーザ情報
     */
    @GetMapping("/get-user-info")
    public UserInfoResponse getUserInfoList(@RequestParam(name = "userId") String userId) {
        UserModel model = this.service.getUserInfo(userId);
        return UserInfoResponse.builder()
            .dispNm(model.getUserInfo().getDispNm())
            .profileImageUrl(this.utility.image2Url(model.getUserInfo().getProfileImage(), model.getUserInfo().getImageType()))
            .state(model.getUserAddressInfo().getState())
            .city(model.getUserAddressInfo().getCity())
            .station(model.getUserAddressInfo().getStation())
            .locale(model.getUserSettingInfo().getLocale())
            .searchTag(model.getUserSettingInfo().getSearchTag())
            .portfolioUrl(model.getUserSettingInfo().getPortfolioUrl())
            .build();
    }
}
