package com.portfolio.jinkan.application.payload;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class UserInfoResponse {

    /** 表示名 */
    private String dispNm;

    /** プロフィール画像 */
    private String profileImageUrl;

    /** 都道府県 */
    private String state;

    /** 市区町村 */
    private String city;

    /** 最寄駅 */
    private String station;

    /** 言語 */
    private String locale;

    /** 検索タグ */
    private String searchTag;

    /** ポートフォリオURL */
    private String portfolioUrl;
}
