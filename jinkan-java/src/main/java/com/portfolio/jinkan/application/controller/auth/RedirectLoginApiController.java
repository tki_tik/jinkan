package com.portfolio.jinkan.application.controller.auth;

import static com.portfolio.jinkan.common.Constant.QUERY_KEY_CLIENT_ID;
import static com.portfolio.jinkan.common.Constant.REDIRECT_PREFIX;

import java.net.URI;

import com.portfolio.jinkan.common.configuration.AuthapiConfiguration;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RequestMapping("/auth")
@Controller
public class RedirectLoginApiController {

    private final AuthapiConfiguration configuration;

    /**
     * ログイン用API遷移。
     * 
     * @return ログイン用APIリダイレクト
     */
    @GetMapping("/redirect-login-api")
    public String redirectLoginApi() {
        return REDIRECT_PREFIX + this.createUrl();
    }

    private URI createUrl() {
        return UriComponentsBuilder.fromHttpUrl(this.configuration.getLoginUrl())
            .queryParam(QUERY_KEY_CLIENT_ID, this.configuration.getClientId())
            .build(true)
            .toUri();
    }
}
