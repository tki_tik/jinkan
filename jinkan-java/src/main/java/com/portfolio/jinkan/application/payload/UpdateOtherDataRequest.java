package com.portfolio.jinkan.application.payload;

import lombok.Data;

@Data
public class UpdateOtherDataRequest {

    /** ユーザID */
    private String userId;

    /** 言語 */
    private String locale;

    /** 検索タグ */
    private String searchTag;

    /** ポートフォリオURL */
    private String portfolioUrl;
}
