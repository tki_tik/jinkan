package com.portfolio.jinkan.application.controller.api;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api")
@RestController
public class RefreshTokenController {

    /**
     * token更新用コントローラ
     */
    @PostMapping("/refresh-token")
    public void refreshToken() {
    }
}
