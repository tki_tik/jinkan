package com.portfolio.jinkan.application.controller.auth;

import javax.servlet.http.HttpServletResponse;

import com.portfolio.jinkan.common.JWTProvider;
import com.portfolio.jinkan.domain.model.AuthInfoModel;
import com.portfolio.jinkan.domain.service.GetAuthInfoService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RequestMapping("/auth")
@RestController
public class LoginController {

    private final GetAuthInfoService service;

    private final JWTProvider provider;

    /**
     * ログイン。
     * 
     * @param response HttpServletResponse
     * @param code     認可コード
     * @return リダイレクト先
     */
    @GetMapping("/login")
    public void login(HttpServletResponse response, @RequestParam(name = "code") String code) {
        AuthInfoModel authInfo = this.service.getAuthInfo(code);
        this.provider.setToken(response, this.provider.createToken(authInfo));
    }
}
