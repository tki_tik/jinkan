package com.portfolio.jinkan.application.payload;

import java.util.List;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class MenuAuthResponse {

    /** 権限リスト */
    private List<String> authList;
}
