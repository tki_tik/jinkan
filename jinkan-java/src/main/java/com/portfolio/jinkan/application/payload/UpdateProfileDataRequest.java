package com.portfolio.jinkan.application.payload;

import lombok.Data;

@Data
public class UpdateProfileDataRequest {

    /** ユーザID */
    private String userId;

    /** 表示名 */
    private String dispNm;

    /** 都道府県 */
    private String state;

    /** 市区町村 */
    private String city;

    /** 最寄駅 */
    private String station;
}
