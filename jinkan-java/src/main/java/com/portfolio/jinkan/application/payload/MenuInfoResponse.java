package com.portfolio.jinkan.application.payload;

import java.util.List;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class MenuInfoResponse {

    /** メニュー情報リスト */
    private List<MenuInfo> menuInfoList;
}
