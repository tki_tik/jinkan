package com.portfolio.jinkan.application.controller.auth;

import com.portfolio.jinkan.domain.service.LogoutService;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RequestMapping("/auth")
@RequiredArgsConstructor
@RestController
public class LogoutController {

    private final LogoutService service;

    /**
     * ログアウト。
     */
    @PostMapping("/logout")
    public void logout() {
        this.service.doLogout();
    }
}
