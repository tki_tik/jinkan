package com.portfolio.jinkan.application.controller.api;

import com.portfolio.jinkan.application.payload.UpdateProfileDataRequest;
import com.portfolio.jinkan.domain.model.UpdateProfileDataModel;
import com.portfolio.jinkan.domain.service.UpdateProfileDataService;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RequestMapping("/api")
@RestController
public class UpdateProfileDataController {

    private final UpdateProfileDataService service;

    /**
     * プロファイル情報更新。
     * 
     * @param request プロファイル情報
     */
    @PostMapping("/update-profile-data")
    public void updateProfileData(@RequestBody UpdateProfileDataRequest request) {
        this.service.updateProfile(UpdateProfileDataModel.builder()
            .userId(request.getUserId())
            .dispNm(request.getDispNm())
            .state(request.getState())
            .city(request.getCity())
            .station(request.getStation())
            .build()
        );
    }
}
