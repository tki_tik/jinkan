package com.portfolio.jinkan.application.controller.api;

import com.portfolio.jinkan.application.payload.UpdateOtherDataRequest;
import com.portfolio.jinkan.domain.model.UpdateOtherDataModel;
import com.portfolio.jinkan.domain.service.UpdateOtherDataService;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RequestMapping("/api")
@RestController
public class UpdateOtherDataController {

    private final UpdateOtherDataService service;

    /**
     * その他情報更新。
     * 
     * @param request その他情報
     */
    @PostMapping("/update-other-data")
    public void updateOtherData(@RequestBody UpdateOtherDataRequest request) {
        this.service.updateOther(UpdateOtherDataModel.builder()
            .userId(request.getUserId())
            .locale(request.getLocale())
            .searchTag(request.getSearchTag())
            .portfolioUrl(request.getPortfolioUrl())
            .build()
        );
    }
}
