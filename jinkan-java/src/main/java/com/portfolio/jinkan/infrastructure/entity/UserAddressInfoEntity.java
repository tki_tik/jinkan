package com.portfolio.jinkan.infrastructure.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.portfolio.jinkan.domain.model.UserAddressInfoModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_jin_user_address_info")
public class UserAddressInfoEntity extends AbstractEntity {

    /** ユーザID */
    @Id
    @Column(length = 6, nullable = false)
    private String userId;

    /** 都道府県 */
    @Column(length = 63, nullable = false)
    private String state;

    /** 都道府県 */
    @Column(length = 63, nullable = false)
    private String city;

    /** 最寄駅 */
    @Column(length = 63, nullable = false)
    private String station;

    /**
     * ユーザ住所情報モデル生成。
     * 
     * @return ユーザ住所情報モデル
     */
    public UserAddressInfoModel toUserAddressInfoModel() {
        return UserAddressInfoModel.builder()
            .state(this.state)
            .city(this.city)
            .station(this.station)
            .build();
    }
}
