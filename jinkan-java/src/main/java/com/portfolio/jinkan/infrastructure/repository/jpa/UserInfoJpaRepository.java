package com.portfolio.jinkan.infrastructure.repository.jpa;

import com.portfolio.jinkan.infrastructure.entity.UserInfoEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface UserInfoJpaRepository extends JpaRepository<UserInfoEntity, String> {

    /**
     * プロフィール画像更新。
     * 
     * @param profileImage プロフィール画像
     * @param type         プロフィール画像拡張子
     * @param userId       ユーザID
     * @return 更新件数
     */
    @Transactional
    @Modifying
    @Query("update UserInfoEntity u set "
        + " u.profileImage = :profileImage, "
        + " u.imageType = :type, "
        + " u.updatedUserId = :userId, "
        + " u.updatedAt = now() "
        + " where u.userId = :userId ")
    public int updateProfileImageAndImageType(@Param("profileImage") byte[] profileImage,
        @Param("type") String type, @Param("userId") String userId);

    /**
     * 表示名更新。
     * 
     * @param dispNm 表示名
     * @param userId ユーザID
     * @return 更新件数
     */
    @Transactional
    @Modifying
    @Query("update UserInfoEntity u set "
        + " u.dispNm = :dispNm, "
        + " u.updatedUserId = :userId, "
        + " u.updatedAt = now() "
        + " where u.userId = :userId ")
    public int updateDispNm(@Param("dispNm") String dispNm, @Param("userId") String userId);
}
