package com.portfolio.jinkan.infrastructure.repository.jpa;

import javax.transaction.Transactional;

import com.portfolio.jinkan.infrastructure.entity.UserSettingInfoEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserSettingInfoJpaRepository extends JpaRepository<UserSettingInfoEntity, String> {

    /**
     * ユーザ設定情報更新。
     * 
     * @param locale        言語
     * @param searchTag    検索タグ
     * @param portfolioUrl ポートフォリオURL
     * @param userId        ユーザID
     * @return 更新件数
     */
    @Transactional
    @Modifying
    @Query("update UserSettingInfoEntity u set "
        + " u.locale = :locale, "
        + " u.searchTag = :searchTag, "
        + " u.portfolioUrl = :portfolioUrl, "
        + " u.updatedUserId = :userId, "
        + " u.updatedAt = now() "
        + " where u.userId = :userId ")
    public int update(@Param("locale") String locale, @Param("searchTag") String searchTag,
        @Param("portfolioUrl") String portfolioUrl, @Param("userId") String userId);
}
