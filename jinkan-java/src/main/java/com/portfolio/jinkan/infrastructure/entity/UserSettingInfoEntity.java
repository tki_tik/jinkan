package com.portfolio.jinkan.infrastructure.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.portfolio.jinkan.domain.model.UserSettingInfoModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_jin_user_setting_info")
public class UserSettingInfoEntity extends AbstractEntity {

    /** ユーザID */
    @Id
    @Column(length = 6, nullable = false)
    private String userId;

    /** 言語 */
    @Column(length = 2, nullable = false)
    private String locale;

    /** 検索タグ */
    @Column(length = 63)
    private String searchTag;

    /** ポートフォリオURL */
    @Column(length = 255)
    private String portfolioUrl;

    /**
     * ユーザ設定情報モデル生成。
     * 
     * @return ユーザ設定情報モデル
     */
    public UserSettingInfoModel toUserSettingInfoModel() {
        return UserSettingInfoModel.builder()
            .locale(this.locale)
            .searchTag(this.searchTag)
            .portfolioUrl(this.portfolioUrl)
            .build();
    }
}
