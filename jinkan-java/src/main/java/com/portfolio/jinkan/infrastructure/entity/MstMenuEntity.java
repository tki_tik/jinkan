package com.portfolio.jinkan.infrastructure.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.portfolio.jinkan.domain.model.MenuInfoModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_jin_mst_menu")
public class MstMenuEntity extends AbstractEntity {

    /** メニューID */
    @Id
    @Column(length = 3, nullable = false)
    private String id;

    /** メニューURL */
    @Column(length = 255, nullable = false)
    private String url;

    /** メニューSVGパス */
    @Column(nullable = false)
    private String path;

    /** メニュータイトルタグ */
    @Column(length = 64, nullable = false)
    private String titleTag;

    /** 権限 */
    @Column(length = 64, nullable = false)
    private String authority;

    /** 表示フラグ */
    @Column(length = 1, nullable = false)
    private String dispFlg;

    /**
     * メニュー情報モデル生成。
     * 
     * @return メニュー情報モデル
     */
    public MenuInfoModel toMenuInfoModel() {
        return MenuInfoModel.builder()
            .id(this.id)
            .url(this.url)
            .path(this.path)
            .titleTag(this.titleTag)
            .build();
    }
}
