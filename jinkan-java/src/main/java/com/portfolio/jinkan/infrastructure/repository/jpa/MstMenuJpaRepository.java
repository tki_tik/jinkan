package com.portfolio.jinkan.infrastructure.repository.jpa;

import java.util.List;

import com.portfolio.jinkan.infrastructure.entity.MstMenuEntity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MstMenuJpaRepository extends JpaRepository<MstMenuEntity, String> {

    public List<MstMenuEntity> findByTitleTagOrderByAuthority(String titleTag);

    public List<MstMenuEntity> findByAuthorityAndDispFlgOrderById(String authority, String dispFlg);
}
