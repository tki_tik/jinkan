package com.portfolio.jinkan.infrastructure.client.authapi;

import static com.portfolio.jinkan.common.Constant.QUERY_KEY_CODE;

import java.net.URI;

import com.portfolio.jinkan.common.configuration.AuthapiConfiguration;
import com.portfolio.jinkan.infrastructure.dto.authapi.AuthInfo;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component
public class GetAuthInfoClient {

    private final RestTemplate restTemplate;

    private final AuthapiConfiguration configuration;

    /**
     * 認証情報取得。
     * 
     * @param code 認可コード
     * @return 認証情報
     */
    public AuthInfo getAuthInfo(String code) {
        return this.execute(code);
    }

    private AuthInfo execute(String code) {
        AuthInfo res = this.restTemplate.getForObject(
            this.createUrl(code),
            AuthInfo.class
        );
        return res;
    }

    private URI createUrl(String code) {
        return UriComponentsBuilder.fromHttpUrl(this.configuration.getGetAuthInfoUrl())
            .queryParam(QUERY_KEY_CODE, code).build().toUri();
    }
}
