package com.portfolio.jinkan.infrastructure.repository.impl;

import java.io.IOException;

import com.portfolio.jinkan.domain.model.UserInfoModel;
import com.portfolio.jinkan.domain.repository.UserInfoRepository;
import com.portfolio.jinkan.infrastructure.entity.UserInfoEntity;
import com.portfolio.jinkan.infrastructure.repository.jpa.UserInfoJpaRepository;

import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Repository
public class UserInfoRepositoryImpl implements UserInfoRepository {

    private final UserInfoJpaRepository jpaRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public UserInfoModel getUserInfo(String userId) {
        UserInfoEntity entity = this.jpaRepository.findById(userId).orElseThrow(RuntimeException::new);

        return entity.toUserInfoModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int updateProfileImage(String userId) throws IOException {

        return this.jpaRepository.updateProfileImageAndImageType(null, null, userId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int updateProfileImage(String userId, MultipartFile profileImage) throws IOException {

        return this.jpaRepository.updateProfileImageAndImageType(
            profileImage.getBytes(), profileImage.getContentType(), userId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int updateDispNm(String userId, String dispNm) {

        return this.jpaRepository.updateDispNm(dispNm, userId);
    }
}
