package com.portfolio.jinkan.infrastructure.repository.impl;

import com.portfolio.jinkan.domain.model.UpdateOtherDataModel;
import com.portfolio.jinkan.domain.model.UserSettingInfoModel;
import com.portfolio.jinkan.domain.repository.UserSettingInfoRepository;
import com.portfolio.jinkan.infrastructure.entity.UserSettingInfoEntity;
import com.portfolio.jinkan.infrastructure.repository.jpa.UserSettingInfoJpaRepository;

import org.springframework.stereotype.Repository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Repository
public class UserSettingInfoRepositoryImpl implements UserSettingInfoRepository {

    private final UserSettingInfoJpaRepository jpaRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public UserSettingInfoModel getUserSettingInfo(String userId) {
        UserSettingInfoEntity entity =
            this.jpaRepository.findById(userId).orElseThrow(RuntimeException::new);

        return entity.toUserSettingInfoModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int update(UpdateOtherDataModel model) {
        return this.jpaRepository.update(
            model.getLocale(),
            model.getSearchTag(),
            model.getPortfolioUrl(),
            model.getUserId()
        );
    }
}
