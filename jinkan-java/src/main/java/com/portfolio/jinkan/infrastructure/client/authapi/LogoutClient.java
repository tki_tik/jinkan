package com.portfolio.jinkan.infrastructure.client.authapi;

import java.net.URI;

import com.portfolio.jinkan.common.configuration.AuthapiConfiguration;

import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component
public class LogoutClient {

    private final RestTemplate restTemplate;

    private final AuthapiConfiguration configuration;

    /**
     * ログアウト。
     */
    public void logout() {
        this.execute();
    }

    private void execute() {
        this.restTemplate.exchange(
            URI.create(this.configuration.getLogoutUrl()),
            HttpMethod.POST,
            null,
            Void.class
        );
    }
}
