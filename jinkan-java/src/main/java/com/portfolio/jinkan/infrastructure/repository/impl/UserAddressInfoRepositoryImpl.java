package com.portfolio.jinkan.infrastructure.repository.impl;

import com.portfolio.jinkan.domain.model.UpdateProfileDataModel;
import com.portfolio.jinkan.domain.model.UserAddressInfoModel;
import com.portfolio.jinkan.domain.repository.UserAddressInfoRepository;
import com.portfolio.jinkan.infrastructure.entity.UserAddressInfoEntity;
import com.portfolio.jinkan.infrastructure.repository.jpa.UserAddressInfoJpaRepository;

import org.springframework.stereotype.Repository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Repository
public class UserAddressInfoRepositoryImpl implements UserAddressInfoRepository {

    private final UserAddressInfoJpaRepository jpaRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public UserAddressInfoModel getUserAddressInfo(String userId) {
        UserAddressInfoEntity entity =
            this.jpaRepository.findById(userId).orElseThrow(RuntimeException::new);

        return entity.toUserAddressInfoModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int update(UpdateProfileDataModel model) {
        return this.jpaRepository.update(
            model.getState(),
            model.getCity(),
            model.getStation(),
            model.getUserId()
        );
    }
}
