package com.portfolio.jinkan.infrastructure.repository.impl.client;

import com.portfolio.jinkan.domain.repository.client.LogoutRepository;
import com.portfolio.jinkan.infrastructure.client.authapi.LogoutClient;

import org.springframework.stereotype.Repository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Repository
public class LogoutRepositoryImpl implements LogoutRepository {

    private final LogoutClient client;

    /**
     * {@inheritDoc}
     */
    @Override
    public void logout() {
        this.client.logout();
    }
}
