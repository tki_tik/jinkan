package com.portfolio.jinkan.infrastructure.repository.jpa;

import com.portfolio.jinkan.infrastructure.entity.UserAddressInfoEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface UserAddressInfoJpaRepository extends JpaRepository<UserAddressInfoEntity, String> {

    /**
     * ユーザ住所情報更新。
     * 
     * @param state   都道府県
     * @param city    市区町村
     * @param station 最寄駅
     * @param userId  ユーザID
     * @return 更新件数
     */
    @Transactional
    @Modifying
    @Query("update UserAddressInfoEntity u set "
        + " u.state = :state, "
        + " u.city = :city, "
        + " u.station = :station, "
        + " u.updatedUserId = :userId, "
        + " u.updatedAt = now() "
        + " where u.userId = :userId ")
    public int update(@Param("state") String state, @Param("city") String city,
        @Param("station") String station, @Param("userId") String userId);
}
