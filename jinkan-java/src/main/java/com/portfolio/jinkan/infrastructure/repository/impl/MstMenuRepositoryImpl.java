package com.portfolio.jinkan.infrastructure.repository.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.portfolio.jinkan.domain.model.MenuInfoModel;
import com.portfolio.jinkan.domain.repository.MstMenuRepository;
import com.portfolio.jinkan.infrastructure.entity.MstMenuEntity;
import com.portfolio.jinkan.infrastructure.repository.jpa.MstMenuJpaRepository;

import org.springframework.stereotype.Repository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Repository
public class MstMenuRepositoryImpl implements MstMenuRepository {

    private final MstMenuJpaRepository jpaRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getAuthorityList(String titleTag) {
        List<MstMenuEntity> entityList = this.jpaRepository.findByTitleTagOrderByAuthority(titleTag);
        return entityList.stream().map(e -> e.getAuthority()).collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MenuInfoModel> getMenuInfoList(String auth) {
        List<MstMenuEntity> entityList = this.jpaRepository.findByAuthorityAndDispFlgOrderById(auth, "1");
        List<MenuInfoModel> modelList = new ArrayList<>();
        for (MstMenuEntity entity : entityList) {
            modelList.add(entity.toMenuInfoModel());
        }
        return modelList;
    }
}
