package com.portfolio.jinkan.infrastructure.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.portfolio.jinkan.domain.model.UserInfoModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_jin_user_info")
public class UserInfoEntity extends AbstractEntity {

    /** ユーザID */
    @Id
    @Column(length = 6, nullable = false)
    private String userId;

    /** 表示名 */
    @Column(length = 63)
    private String dispNm;

    /** プロフィール画像 */
    @Column
    private byte[] profileImage;

    /** プロフィール画像拡張子 */
    @Column(length = 63)
    private String imageType;

    /**
     * ユーザ情報モデル生成。
     * 
     * @return ユーザ情報モデル
     */
    public UserInfoModel toUserInfoModel() {
        return UserInfoModel.builder()
            .dispNm(this.dispNm)
            .profileImage(this.profileImage)
            .imageType(this.imageType)
            .build();
    }
}
