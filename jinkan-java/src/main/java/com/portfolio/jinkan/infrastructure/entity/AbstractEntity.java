package com.portfolio.jinkan.infrastructure.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
public class AbstractEntity implements Serializable {

    /** 作成ユーザID */
    @Column(insertable = true, updatable = false, length = 100)
    private String createdUserId;

    /** 作成日時 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(insertable = true, updatable = false)
    private Date createdAt;

    /** 更新ユーザID */
    @Column(insertable = true, updatable = true, length = 100)
    private String updatedUserId;

    /** 更新日時 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(insertable = true, updatable = true)
    private Date updatedAt;
}
