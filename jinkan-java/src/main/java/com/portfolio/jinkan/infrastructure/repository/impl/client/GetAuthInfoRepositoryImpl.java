package com.portfolio.jinkan.infrastructure.repository.impl.client;

import com.portfolio.jinkan.domain.model.AuthInfoModel;
import com.portfolio.jinkan.domain.repository.client.GetAuthInfoRepository;
import com.portfolio.jinkan.infrastructure.client.authapi.GetAuthInfoClient;
import com.portfolio.jinkan.infrastructure.dto.authapi.AuthInfo;

import org.springframework.stereotype.Repository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Repository
public class GetAuthInfoRepositoryImpl implements GetAuthInfoRepository {

    private final GetAuthInfoClient client;

    /**
     * {@inheritDoc}
     */
    @Override
    public AuthInfoModel getAuthInfo(String code) {
        AuthInfo info = this.client.getAuthInfo(code);
        return AuthInfoModel.builder()
            .userId(info.getUserId())
            .userName(info.getUserName())
            .role(info.getRole())
            .build();
    }
}
