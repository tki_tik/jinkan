package com.portfolio.jinkan.domain.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class MenuInfoModel {

    /** メニューID */
    private String id;

    /** メニューURL */
    private String url;

    /** メニューアイコンPATH */
    private String path;

    /** メニュータイトルタグ */
    private String titleTag;
}
