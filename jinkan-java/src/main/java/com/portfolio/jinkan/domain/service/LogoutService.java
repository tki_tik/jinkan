package com.portfolio.jinkan.domain.service;

import com.portfolio.jinkan.domain.repository.client.LogoutRepository;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class LogoutService {

    private final LogoutRepository repository;

    /**
     * ログアウト。
     */
    public void doLogout() {
        this.repository.logout();
    }
}
