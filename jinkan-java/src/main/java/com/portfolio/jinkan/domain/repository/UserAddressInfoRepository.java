package com.portfolio.jinkan.domain.repository;

import com.portfolio.jinkan.domain.model.UpdateProfileDataModel;
import com.portfolio.jinkan.domain.model.UserAddressInfoModel;

public interface UserAddressInfoRepository {

    /**
     * ユーザ住所情報取得。
     * 
     * @param userId ユーザID
     * @return ユーザ住所情報
     */
    public UserAddressInfoModel getUserAddressInfo(String userId);

    /**
     * ユーザ住所情報更新。
     * 
     * @param model ユーザ住所情報
     * @return 更新件数
     */
    public int update(UpdateProfileDataModel model);
}
