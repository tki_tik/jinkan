package com.portfolio.jinkan.domain.repository.client;

public interface LogoutRepository {

    /**
     * ログアウト。
     */
    public void logout();
}
