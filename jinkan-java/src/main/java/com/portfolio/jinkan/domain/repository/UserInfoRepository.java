package com.portfolio.jinkan.domain.repository;

import java.io.IOException;

import com.portfolio.jinkan.domain.model.UserInfoModel;

import org.springframework.web.multipart.MultipartFile;

public interface UserInfoRepository {

    /**
     * ユーザ情報取得。
     * 
     * @param userId ユーザID
     * @return ユーザ情報
     */
    public UserInfoModel getUserInfo(String userId);

    /**
     * プロフィール画像更新。
     * 
     * @param userId ユーザID
     * @return 更新件数
     * @throws IOException バイナリ変換に失敗した場合
     */
    public int updateProfileImage(String userId) throws IOException;

    /**
     * プロフィール画像更新。
     * 
     * @param userId       ユーザID
     * @param profileImage プロフィール画像
     * @return 更新件数
     * @throws IOException バイナリ変換に失敗した場合
     */
    public int updateProfileImage(String userId, MultipartFile profileImage) throws IOException;

    /**
     * 表示名更新。
     * 
     * @param userId ユーザID
     * @param dispNm 表示名
     * @return 更新件数
     */
    public int updateDispNm(String userId, String dispNm);
}
