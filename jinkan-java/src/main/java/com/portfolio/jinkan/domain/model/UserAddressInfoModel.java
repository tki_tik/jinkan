package com.portfolio.jinkan.domain.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class UserAddressInfoModel {

    /** 都道府県 */
    private String state;

    /** 都道府県 */
    private String city;

    /** 最寄駅 */
    private String station;
}
