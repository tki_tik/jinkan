package com.portfolio.jinkan.domain.repository;

import com.portfolio.jinkan.domain.model.UpdateOtherDataModel;
import com.portfolio.jinkan.domain.model.UserSettingInfoModel;

public interface UserSettingInfoRepository {

    /**
     * ユーザ設定情報取得。
     * 
     * @param userId ユーザID
     * @return ユーザ設定情報
     */
    public UserSettingInfoModel getUserSettingInfo(String userId);

    /**
     * ユーザ設定情報更新。
     * 
     * @param model ユーザ設定情報
     * @return 更新件数
     */
    public int update(UpdateOtherDataModel model);
}
