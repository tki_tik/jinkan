package com.portfolio.jinkan.domain.service;

import com.portfolio.jinkan.domain.model.UpdateOtherDataModel;
import com.portfolio.jinkan.domain.repository.UserSettingInfoRepository;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class UpdateOtherDataService {

    private final UserSettingInfoRepository repository;

    /**
     * ユーザその他情報更新。
     * 
     * @param model ユーザその他情報
     */
    public void updateOther(UpdateOtherDataModel model) {

        int updateCnt = this.repository.update(model);

        if (updateCnt == 0) {
            throw new RuntimeException();
        }
    }
}
