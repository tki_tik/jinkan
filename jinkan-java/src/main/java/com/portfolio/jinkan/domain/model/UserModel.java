package com.portfolio.jinkan.domain.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class UserModel {

    /** ユーザ情報 */
    private UserInfoModel userInfo;

    /** ユーザ情報 */
    private UserAddressInfoModel userAddressInfo;

    /** ユーザ情報 */
    private UserSettingInfoModel userSettingInfo;
}
