package com.portfolio.jinkan.domain.service;

import com.portfolio.jinkan.domain.model.UpdateProfileDataModel;
import com.portfolio.jinkan.domain.repository.UserAddressInfoRepository;
import com.portfolio.jinkan.domain.repository.UserInfoRepository;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class UpdateProfileDataService {

    private final UserInfoRepository userInfoRepository;

    private final UserAddressInfoRepository userAddressInfoRepository;

    /**
     * ユーザ住所情報更新。
     * 
     * @param model ユーザ住所情報
     */
    public void updateProfile(UpdateProfileDataModel model) {

        int updateCnt = 0;

        updateCnt = this.userInfoRepository.updateDispNm(model.getUserId(), model.getDispNm());

        if (updateCnt == 0) {
            throw new RuntimeException();
        }

        updateCnt = this.userAddressInfoRepository.update(model);

        if (updateCnt == 0) {
            throw new RuntimeException();
        }
    }
}
