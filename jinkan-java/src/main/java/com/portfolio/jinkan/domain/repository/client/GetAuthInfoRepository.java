package com.portfolio.jinkan.domain.repository.client;

import com.portfolio.jinkan.domain.model.AuthInfoModel;

public interface GetAuthInfoRepository {

    /**
     * 認証情報取得。
     * 
     * @param code 認可コード
     * @return 認証情報
     */
    public AuthInfoModel getAuthInfo(String code);
}
