package com.portfolio.jinkan.domain.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class UpdateOtherDataModel {

    /** ユーザID */
    private String userId;

    /** 言語 */
    private String locale;

    /** 検索タグ */
    private String searchTag;

    /** ポートフォリオURL */
    private String portfolioUrl;
}
