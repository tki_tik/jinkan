package com.portfolio.jinkan.domain.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class UserInfoModel {

    /** 表示名 */
    private String dispNm;

    /** プロフィール画像 */
    private byte[] profileImage;

    /** プロフィール画像拡張子 */
    private String imageType;
}
