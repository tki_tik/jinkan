package com.portfolio.jinkan.domain.repository;

import java.util.List;

import com.portfolio.jinkan.domain.model.MenuInfoModel;

public interface MstMenuRepository {

    /**
     * 権限取得。
     * 
     * @param titleTag メニュータイトルタグ
     * @return 権限リスト
     */
    public List<String> getAuthorityList(String titleTag);

    /**
     * メニュー情報取得。
     * 
     * @param auth 権限
     * @return メニュー情報リスト
     */
    public List<MenuInfoModel> getMenuInfoList(String auth);
}
