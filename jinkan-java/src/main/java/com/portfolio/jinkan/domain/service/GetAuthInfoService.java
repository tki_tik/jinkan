package com.portfolio.jinkan.domain.service;

import com.portfolio.jinkan.domain.model.AuthInfoModel;
import com.portfolio.jinkan.domain.repository.client.GetAuthInfoRepository;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class GetAuthInfoService {

    private final GetAuthInfoRepository repository;

    /**
     * 認証情報取得。
     * 
     * @param code       認可コード
     * @param jsessionid JSESSIONID
     * @return 認証情報
     */
    public AuthInfoModel getAuthInfo(String code) {
        return this.repository.getAuthInfo(code);
    }
}
