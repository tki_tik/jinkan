package com.portfolio.jinkan.domain.service;

import java.util.List;

import com.portfolio.jinkan.domain.model.MenuInfoModel;
import com.portfolio.jinkan.domain.repository.MstMenuRepository;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class GetMenuInfoService {

    private final MstMenuRepository repository;

    /**
     * メニュー権限リスト取得。
     * 
     * @param titleTag メニュータイトルタグ
     * @return メニュー権限リスト
     */
    public List<String> getMenuAuthList(String titleTag) {
        return this.repository.getAuthorityList(titleTag);
    }

    /**
     * メニュー情報リスト取得
     * 
     * @param auth 権限
     * @return メニュー情報リスト
     */
    public List<MenuInfoModel> getMenuInfoList(String auth) {
        return this.repository.getMenuInfoList(auth);
    }
}
