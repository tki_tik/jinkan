package com.portfolio.jinkan.domain.service;

import java.io.IOException;

import com.portfolio.jinkan.domain.repository.UserInfoRepository;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class UploadProfileImageService {

    private final UserInfoRepository repository;

    /**
     * プロフィール画像更新。
     * 
     * @param userId ユーザID
     * @param profileImage プロフィール画像
     * @throws IOException バイナリ変換に失敗した場合
     */
    public void uploadProfileImage(String userId, MultipartFile profileImage) throws IOException {
        int updateCnt = 0;
        if (profileImage == null) {
            updateCnt = this.repository.updateProfileImage(userId);
        } else {
            updateCnt = this.repository.updateProfileImage(userId, profileImage);
        }
        if (updateCnt == 0) {
            throw new RuntimeException();
        }
    }
}
