package com.portfolio.jinkan.domain.service;

import com.portfolio.jinkan.domain.model.UserModel;
import com.portfolio.jinkan.domain.repository.UserAddressInfoRepository;
import com.portfolio.jinkan.domain.repository.UserInfoRepository;
import com.portfolio.jinkan.domain.repository.UserSettingInfoRepository;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class GetUserInfoService {

    private final UserInfoRepository userInfoRepository;

    private final UserAddressInfoRepository userAddressInfoRepository;

    private final UserSettingInfoRepository userSettingInfoRepository;

    /**
     * ユーザ情報取得。
     * 
     * @param userId ユーザID
     * @return ユーザ情報
     */
    public UserModel getUserInfo(String userId) {
        return UserModel.builder()
            .userInfo(this.userInfoRepository.getUserInfo(userId))
            .userAddressInfo(this.userAddressInfoRepository.getUserAddressInfo(userId))
            .userSettingInfo(this.userSettingInfoRepository.getUserSettingInfo(userId))
            .build();
    }
}
