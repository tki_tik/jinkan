package com.portfolio.jinkan.domain.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class UpdateProfileDataModel {

    /** ユーザID */
    private String userId;

    /** 表示名 */
    private String dispNm;

    /** 都道府県 */
    private String state;

    /** 市区町村 */
    private String city;

    /** 最寄駅 */
    private String station;
}
