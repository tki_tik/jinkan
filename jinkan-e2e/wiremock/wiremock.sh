#!/bin/sh

# Kill any existing instances of "wiremock"
ps -Af | grep java | grep "wiremock" | awk '{print $2}' | xargs kill 2>/dev/null
if [ "$1" = "shutdown" ]; then echo "Goodbye!"; exit 0; fi;

# start wiremock
nohup java -jar -Xms512m -Xmx512m -XX:NewSize=256m -XX:MaxNewSize=256m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=128m -XX:CompressedClassSpaceSize=128m -XX:InitialCodeCacheSize=96m -XX:ReservedCodeCacheSize=96m -XX:+UseConcMarkSweepGC -XX:+CMSParallelRemarkEnabled -XX:+UseCMSInitiatingOccupancyOnly -XX:+CMSClassUnloadingEnabled -XX:+ExplicitGCInvokesConcurrent -XX:+DisableExplicitGC -XX:CMSInitiatingOccupancyFraction=80 -XX:+CMSScavengeBeforeRemark -verbose:gc -XX:+PrintGCDetails -Xloggc:./logs/gc.log ./wiremock-jre8-standalone-2.28.0.jar --bind-address 0.0.0.0 --port 14400 --enable-stub-cors --no-request-journal --print-all-network-traffic --verbose  >& ./logs/stdout_`date "+%Y%m%d%H%M%S"`.log &
